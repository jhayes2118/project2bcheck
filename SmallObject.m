//
//  SmallObject.m
//  2dGame
//
//  Created by Joseph Reiter on 11/4/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "SmallObject.h"


@implementation SmallObject
@synthesize  description, interactable;


-(id)init{
    self = [super init];
    description = @"blank";
    return self;
}
- (id)initWithDescription:(NSString*)SmallObjectDescript AndInteractable:(BOOL)actable{
    self = [self init];
    if(self){
        //NSLog(@"<SMALLOBJECT you are setting a description, give yourself a pat on the back: %@", SmallObjectDescript);
        description = SmallObjectDescript;
        //NSLog(@"<SMALLOBJECT description: %@", description);
        interactable = actable;
        
    }
    return self;
}


@end
