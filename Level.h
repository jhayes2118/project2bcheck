//
//  Level.h
//  2dGame
//
//  Created by John Hayess on 11/9/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AudioToolbox/AudioServices.h>
#import "Cell.h"
#import "SmallObject.h"
#import "Player.h"
#import "Monster.h"

@protocol LevelContentsDelegator<NSObject>
-(void)levelHasChanged:(int)currentCell;//for player
-(void)energyHasChanged:(int)currentEnergy;//for player
-(void)coinHasChanged:(int)currentCoin;//for player
-(void)monsterHasMoved:(int)monsterLocation;//for monster
-(void)playerIsInTheBeyond;
-(void)livesHasChanged:(int)currentLives;
-(void)healthHasChanged:(int)currentHealth;
-(void)attackHasChanged:(int)currentAttack;
-(void)defenseHasChanged:(int)currentDefense;
-(void)monsterKilled;
-(void)monstersHealthHasChanged:(int)currentHealth;
@end

@interface Level : NSObject <PlayerLocationDelegate, PlayerEnergyDelegate, MonsterLocationDelegate>{

    int lId;
    int playerCell;
    NSMutableArray *cells;
    NSDictionary *exits;
   // NSMutableArray *locations;
    NSString *description;
    int currentCell;
    int monsterLocation;
    id <LevelContentsDelegator> levelDelegator;
    NSNumber *level;
    NSNumber *cell;
    Monster  *tempMonster;
    BOOL monsterNextToPlayer;
    int monsterNextMove;
}
@property int currentCell;
@property int lId;
@property NSString *description;
@property NSMutableArray *cells;
@property int movementEnergyCost;

@property (nonatomic,retain) id levelDelegator;
//@property NSMutableArray *locations;

-(id)initWithId:(int)ident andWithExitCells:(NSArray*)exits andWithExitLevels:(NSArray*)exitLevels;;

-(void)addNeighbors:(int[])neighbors tocId:(int)cId withNumberOf:(int)n;
-(void)addSingleNeighbor:(int)neighbor tocId:(int)cId;

-(void)moveObject:(int)to;
-(NSMutableArray*)getNeighbors;
-(NSArray*)getExits;
-(NSNumber*)getExit:(int)currentCell;

- (void)setDelegate:(id)newDelegate;

-(void)monsterLocationHasChangedTo:(int)number;

-(void)clearCells;
-(void)playBeep;
-(Player*)getPlayer;

-(void)playerAttack;
-(void)playerIsInTheBeyond;
-(void)monsterKilled;
-(void)monsterHealthHasChangedTo:(int)health;
@end
