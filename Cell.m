//
//  Cell.m
//  2dGame
//
//  Created by Joseph Reiter on 11/4/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "Cell.h"


@implementation Cell

@synthesize cellsObject;
@synthesize playerObject;
@synthesize cellsMonster;
@synthesize  cId;
@synthesize empty;
@synthesize neighbors;
@synthesize hasMonster;
@synthesize hasPlayer;
@synthesize fight;
@synthesize numberOfNeighbors;// this is used for picking a random variable NSArray doesn't keep track of size


/*
 The basic init of any object
 
 */

- (id)init{
    self = [super init];
    return self;
}

/*
 
 create a cell and initilize the essential varaables
 
 */
- (id)initWithId:(int)ident {

    self = [super init];
    
        cId = ident;
        empty = TRUE;
        neighbors = [[NSMutableArray alloc]init];
        playerObject = [[Player alloc]init];
        movementEnergyCost = 1;
        fight = FALSE;
    
    
    return self;
    
    
}

/*
 
 Initialize with an object inside it
 @param ident the cell's id
 @param theObject the object to be placed in the cell
 
 */
- (id)initWithId:(int)ident AndObject:(SmallObject*)theObject {
    
    self = [self initWithId:ident];
    cellsObject  = theObject;
    empty = FALSE;

    return self;
    
    
}

/*
 
 Initialize with the neighbor relationship between cells in place
 @param ident cell's id
 @param theNeighbors an array of numbers corresponding to the cells
 the cell has a neighbor relationship with
 
 */
- (id)initWithId:(int)ident AndNeighbors:(NSArray*)theNeighbors{
    
    self = [self initWithId:ident];
    int l = (sizeof theNeighbors);
    //exceptions or change back to regular c array of ints
    neighbors = [[NSMutableArray alloc] initWithCapacity:l];
    NSNumber *n = [[NSNumber alloc]init];
    for (int i = 0; i < l; i++) {
        n = [theNeighbors objectAtIndex:(i)];
        [neighbors addObject:n];
    }
    return self;
    
}
/*
 
 Initialize with all three neighbors, identifier, and the an object inside
 
 */
-(id)initWithId:(int)ident AndNeighbors:(NSArray*)theNeighbors AndObject:(SmallObject*)theObject{
    self = [self initWithId:ident AndNeighbors:theNeighbors];
    cellsObject = theObject;
    return self;
    
}

/*
 
 Add a neighbor relationship to the cell
 @param int aNeigbor the new neighbor to be added
 
 */
-(void)addNeighbor:(int)aNeighbor{
    if(![self containsNeighbor:aNeighbor]){
        NSNumber *n = [[NSNumber alloc] initWithInt:aNeighbor];
        [neighbors addObject:n];
        numberOfNeighbors++;
    }
}

/*
 
 checks if a particular cell (by it's id) is the neighbor of this cell
 @param int aNeighbor the neighbor cell to be check against
 
 @return true if there is a neighbor relationship
 @return false if no relationship
 
 */
-(BOOL)containsNeighbor:(int)aNeighbor{
    NSNumber *a= [[NSNumber alloc] initWithInt:aNeighbor];
    if([neighbors containsObject:a]){
        return TRUE;
    }
    else {
        return FALSE;
            }
}

/*
 
 This is used for monster's autonomous move
 returns a neighbor 
 
 */
-(int)getRandomNeighbor{
    int r = arc4random_uniform(numberOfNeighbors);
    return [[neighbors objectAtIndex:r] integerValue];
}

-(BOOL)containsSmallObject:(SmallObject*)theObject{
    //eventually write a compareto method and use here
    if([theObject isKindOfClass:[cellsObject class]])
    {
        return TRUE;
    }
    else {return FALSE;
    }
}
/*
 
 Checks if the cell has an object in it
 @return true if the cell has no object
 @return false if the cell has an object
 
 */
-(BOOL)isEmpty{
    return empty;
}

/*
Returns the identifier of the cell
 @return int the identifer
 */

-(int)getId{
    return cId;
}

/*
 Adds an object to the cell if the cell is empty
 in later implementations we would make cellsobject into an 
 array called cellsObjects and have an inventory in them
 
 */

-(void)addObject:(SmallObject *)newObject{
    if (empty == TRUE){
        cellsObject = newObject;
        empty = FALSE;
    }
    else (NSLog(@"<CELL>:addObject Already has object, cannot add more"));

}

/*
 This is the method used to add a player to the cell when he is moving
 
 This method checks if there is a monster already in the cell if there is
 a fight is started
 
 if there is already a player the player is not added. 
 @param newPlayer the Player
 */
-(void)addPlayer:(Player *)newPlayer{
  //  if (hasMonster == FALSE){ //originally if (hasMonster == TRUE)what is this?
    if(hasPlayer == FALSE){
        playerObject = newPlayer;
        [playerObject changeLocation:cId];
        //[playerObject changeEnergy:(playerObject.getPlayerEnergy - movementEnergyCost)];
        NSLog(@"Player added to cell: %i",cId);
        [playerObject setFightDelegate:self];
        hasPlayer = TRUE;
        if(hasMonster){
            [self startFight];
        }
    }
    else (NSLog(@"<CELL>:addPlayer Already has player object, cannot add more"));
}

/*
 This method is used to add a monster to the cell. If there is a monster in it it will not be added
 
 @param theMonster
 */

-(void)addMonster:(Monster*)theMonster{
    if (hasMonster == FALSE){ 
        cellsMonster = theMonster;
        [cellsMonster changeLocation:cId];
        //[playerObject changeEnergy:(playerObject.getPlayerEnergy - movementEnergyCost)];
        NSLog(@"monster added to cell: %i",cId);
        [cellsMonster setFightDelegate:self];
        hasMonster = TRUE;
    }
    else (NSLog(@"<CELL>:addMonster Already has Monster, cannot add more"));
}

/*
 
 removes them monster and passes it to the calling module
 
 @return theCellsMonster
 
 */
-(Monster*)removeMonster{
    if(hasMonster){
        Monster *temp = cellsMonster;
        cellsMonster = NULL;
        hasMonster = FALSE;
        if (temp == NULL){
            NSLog(@"Returning NULL Monster monster == null");//here is the problem
            return temp;
        }
        NSLog(@"Returning monster...successful removeal");
        return temp;
    }
    NSLog(@"returning null in remove Monster EMPTY is true in cell: %i)",cId);
    return NULL;
}

/*
 Checks if there is a monster in the cell
 */
-(BOOL)cellHasMonster{
    return hasMonster;
}
/*
 removes and returns the object in the cell
 */
-(SmallObject*)remove{
    SmallObject *temp = cellsObject;
    cellsObject = NULL;
    empty = TRUE;
    return temp;
}
/*
 Returns the player to move him to a new cell or level
 
 @return cellsPlayer 
 
 @joe Look at this why is this getting rid of the cells object???
 */
-(Player*)removePlayer{

    if(hasPlayer){
    Player *temp = playerObject;
    cellsObject = nil;
    cellsObject = nil;
    playerObject = nil;
    playerObject = nil;
    hasPlayer = FALSE;
    if (temp == NULL){
        NSLog(@"rempove player returns null");//here is the problem 
        return temp;
    }
    NSLog(@"Returning player...successful removeal");
    return temp;
    }
    NSLog(@"returning null in remove player EMPTY is true");
    return NULL;
}
/*
 Method returns the neighbors of this cell.
 */
-(NSMutableArray*)getNeighbors{
    NSMutableArray *a = [[NSMutableArray alloc]initWithArray:neighbors];
    return a;
}

/*
 This starts the fight by telling the monster and the player they are in the same cell
 
 fight is set to true so neither the player nor the monster can move
 */
-(void)startFight{
    if(hasMonster){
    cellsMonster.fighting = TRUE;
    [cellsMonster attack];
    playerObject.fighting = TRUE;
    fight = TRUE;
    NSLog(@"<CELL> {startFight} player.fighting = %i",playerObject.fighting);
    
    
    NSLog(@"Starting the fight hahaha");
    }
}

/*
 This method is called if the player wins the fight
 the player is then allowed to move
 */
-(void)stopFight{
    cellsMonster.fighting = FALSE;
    playerObject.fighting = FALSE;
    fight = FALSE;
    hasMonster = FALSE;
    NSLog(@"Stoping the fight aweeee :/");
}

/*
 Sends the monsters attack to the cells player
 */
-(void)monsterSendAttack:(int)attackValue{
    NSLog(@"<Cell> monsterSendAttack");
    if(fight & hasPlayer){
        NSLog(@"<CELL> fight = true...monster attacking");
        [playerObject recieveAttack:attackValue];
    }
}

/*
 sends the players attack to the monster
 */
-(void)playerSendAttack:(int)attackValue{
    NSLog(@"<CELL> playerSendAttack");
    if(fight & hasMonster){
        NSLog(@"<CELL> fight = true ... player attacking");
        if(cellsMonster.health > 0)
        [cellsMonster recieveAttack:attackValue];
    }

}
/*
 ofiscated method cannot remember if it is used, to scared to delete it
 */

-(void)playerAttack{    // do we really need this? can we just call playerSendAttack?
    NSLog(@"<CELL> sending player attack with attack: %i", playerObject.playerAttack);
    [self playerSendAttack:playerObject.playerAttack];
    //[playerObject sendAttack];
}

/*
 called when the player kills the monster
 stops the fight and removes the monster 
 */
-(void)monsterDie{
    cellsMonster = nil;
    [self stopFight];
}
@end
