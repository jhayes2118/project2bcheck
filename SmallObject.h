//
//  SmallObject.h
//  2dGame
//
//  Created by Joseph Reiter on 11/4/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SmallObject : NSObject {
    
    NSString* description;
    BOOL interactable;
    
}

@property NSString *description;
@property BOOL interactable;


- (id)init;
- (id)initWithDescription:(NSString*)SmallObjectDescript AndInteractable:(BOOL)interactable;
- (void)interact;



@end