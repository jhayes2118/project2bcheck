//
//  Player.m
//  2dGameRemake
//
//  Created by Joseph Reiter on 11/11/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "Player.h"

@implementation Player
@synthesize location, fighting, playerLives, playerHealth, playerAttack, playerDefense;
@synthesize locDelegate, eneDelegate;

/*
 Initalize the default player variables and returns a player instance
 */
-(id)init{
    self = [super init];
    location = 0;
    fighting = FALSE;
    return self;
    
}
/*
This sets the current location of the player. This happens as a result of the player calling the 
 location delegate to level
 Level calls this method after it is verified the player can move to the specified cell
 @param newLocation the new location that the player object is in
 */
-(void)changeLocation:(int)newLocation{
    
    location = newLocation;
    [locDelegate locationHasChangedTo:location];
    //NSLog(@"newlocation: %d",newLocation);
    //NSLog(@"location delegate was called");
}

//The following set the two Delegates the location delegate typically goes to the level
//the energy delegate goes to game to show to the view

- (void)setDelegate:(id)newDelegate {
    locDelegate = newDelegate;
}

- (void)setEneDelegate:(id)newDelegate{
    eneDelegate = newDelegate;
}

////energy
//getter and setters

//(increment is used because it is the only one that applies to this game)

//the player's energy

- (int)getPlayerEnergy{
    return playerEnergy;
}
-(void)incrementPlayerEnergy:(int)setNewEnergy{
    NSLog(@"<PLAYER> energy has changed called with %i", setNewEnergy);
    playerEnergy += setNewEnergy;
    [eneDelegate energyHasChangedTo:playerEnergy];
    NSLog(@"<PLAYER> energy has changed to %i", playerEnergy);

    }



///coin

-(int)getPlayerCoin{
    return playerCoin;
}

-(void)incrementPlayerCoin:(int)setNewCoin{
    NSLog(@"<PLAYER> incrementPlayerCoin %i", setNewCoin);
    playerCoin += setNewCoin;
    [eneDelegate coinHasChangedTo:playerCoin];
    NSLog(@"<PLAYER> incrementPlayerCoin player Coin %i", playerCoin);
}
///lives

-(int)getPlayerLives{
    return playerLives;
}

-(void)incrementLives:(int)setNewLives{
    playerLives += setNewLives;
    [eneDelegate livesHasChangedTo:playerLives];
    NSLog(@"<PLAYER> [DELEGATE] changeLives: %d",playerLives);
}
///health

-(void)incrementHealth:(int)setNewHealth{
    playerHealth += setNewHealth;
    [eneDelegate healthHasChangedTo:playerHealth];
    NSLog(@"<PLAYER> [DELEGATE] changeHealth: %d",playerHealth);
}

-(int)getPlayerHealth{
    return playerHealth;
}
///attack
-(void)incrementAttack:(int)setNewAttack{
    playerAttack += setNewAttack;
    [eneDelegate attackHasChangedTo:playerAttack];
    NSLog(@"<PLAYER> [DELEGATE] changeAttack: %d",playerAttack);
}
-(int)getPlayerAttack{
    return playerAttack;
}
///Defense

-(void)incrementDefense:(int)setNewDefense{
    playerDefense += setNewDefense;
    [eneDelegate defenseHasChangedTo:playerDefense];
    NSLog(@"<PLAYER> [DELEGATE] changeDefense: %d",playerDefense);
}

-(int)getPlayerDefense{
    return playerDefense;
}



/*
 These are the attack methods for player Sent to the containing cell (delegate) to send the fightable 
 to other interactable objects in the cell
 */
-(void)recieveAttack:(int)attackValue{
    if (playerDefense > 0){
        [self incrementDefense:-1];
    }
    
    else{
    playerHealth -= attackValue;
    [eneDelegate healthHasChangedTo:playerHealth];
    }
    if(playerHealth <= 0){
        if(playerLives > 0){
            [self incrementLives:-1];
            
            [self incrementHealth:100];
        }
        else{
        [eneDelegate playerIsInTheBeyond];
        }
    }
    
}

/*
 Recieves the attack from other fighable characters in teh cell (delegate cell) 
 */
-(void)sendAttack{
    NSLog(@"<PLAYER> sending attack BRAHHHH!!!");
    [fightDelegate playerSendAttack:playerAttack];
}


@end
