//
//  Monster.m
//  2dGameRemake
//
//  Created by John Hayess on 11/29/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "Monster.h"
#include <stdlib.h>



@implementation Monster

@synthesize location, fighting, health, attackPower;
@synthesize monLocDelegate;

-(id)init{
    self = [super init];
    return self;
    attackPower = 10;
}


/*
 initializes the monster varaibles and calls the monster AI thread
 no longer used
 */
-(id)initWithDelegate:(id)theDelegate{
    
    self = [self init];
    
    iAMAMonster = @"MONSTER";
    location = 23;
    health = 10;
    attackPower = 10;
    fighting = FALSE;
    
    [self createThread];

     return self;
   
}

/*
 initializes the monster varaibles and calls the monster AI thread
 adds a description (delegate no longer set) must be done using set delegate methods
 this was added when we created more than one delegate cell and level delegate
 */
-(id)initWithDescription:(NSString *)newDescription andDelegate:(id)theDelegate{
    self = [self init];
    description = newDescription;
    //NSLog(@"<MONSTER> Description: %@ and newDescription %@", description, newDescription);
    location = 23;
    health = 100;
    fighting = FALSE;
    [self createThread];
    return self;
}

/*
 creates the AI thread. Sends a timed call to move
 */

-(void)createThread{
    timerThread = [[NSThread alloc]initWithTarget:self selector:@selector(move) object:nil];
    [timerThread start];
    timer = [NSTimer scheduledTimerWithTimeInterval:8 target:self selector:@selector(move) userInfo:nil repeats:YES];
}

/*
 This is the call to move the monster uses a random number to determine which cell to move to. 
 */
-(void)move{
    
    //do a check to see if player has engaged, if not, then you can move
    
    if (!fighting){
        int r = arc4random_uniform(36);
        [monLocDelegate monsterLocationHasChangedTo:r];
        NSLog(@"move called with random # %i", r);
    }
    else {
        [timer invalidate];
        timer = [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(attack) userInfo:nil repeats:YES];
    }
}

/*
Method to directly set the monster's location used by level to place the monster in a specific 
 cell
 */
-(void)changeLocation:(int)newLocation{
    location = newLocation;
    NSLog(@"<monster>newlocation: %d",newLocation);
   NSLog(@"<monster>location delegate was called");
}

/*setter for monster's energy
 monster energy not currently used
 not to be confused with health
 */
-(void)changeEnergy:(int)newEnergy{
    energy = newEnergy;
    NSLog(@"<monster> [energy] newEnergy: %d",newEnergy);
}
/*
 getter for monster's energy
 */

- (int)getMonsterEnergy{
    return energy;
}

/*
 sets the delegate that controls the location of the monster
 
 */
- (void)setLocationDelegate:(id)newDelegate {
    //NSLog(@"<MONSETER> set delegate");
    monLocDelegate = newDelegate;
}

/*
sets the delegate that controls the fighting gameplay
 */

-(void)setFightDelegate:(id)newDelegate{
    monFightDelegate = newDelegate;
}

/*
Attack method for monster. Sends the power to the fighting delegate
 */


-(void)attack{
    [monFightDelegate monsterSendAttack:attackPower];
}
/*
 recieves the attack from the fightable players in the cell 
 decreases the health when hit
 
 if the monster's healh goes below 0 the die method is called in the fighting delegate
 */
-(void)recieveAttack:(int)attackValue{
    health -= attackValue;
    [monLocDelegate monsterHealthHasChangedTo:health];
    NSLog(@"<MONSTER> health = %i", health);
    if(health <= 0){
        NSLog(@"<monster> Dead");
        [monLocDelegate monsterKilled];
        [timer invalidate];
        [monFightDelegate monsterDie];
        [monLocDelegate monsterLocationHasChangedTo:-1];
 
    }
}
/*
 increments the health of the monster
 */

-(void)incrementMonsterHealth:(int)newHealth{
    health += newHealth;
    [monLocDelegate monsterHealthHasChangedTo:health];
}



@end

