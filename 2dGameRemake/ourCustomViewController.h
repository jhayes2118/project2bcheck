//
//  View.h
//  2dGameRemake
//
//  Created by Joseph Reiter on 11/11/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameStartMusic.h"
#import "Game.h"
#import "player.h"
#import "CheatCodeView.h"
#import "playerDeathView.h"
#import "youWinView.h"
#import "bunnyNPCTalk.h"

@interface ourCustomViewController :  UIViewController <ViewUpdate> {
    
    //BOOL for things we only want to happen once
    //BOOLS to control views
    BOOL initialSegue;
    BOOL didCheat;
    BOOL playerIsDead;
    BOOL playerWins;
    BOOL bunnyClicked;
    
    //BOOLS to show initial values before delegates take over
    //Player
    BOOL initialLives;
    BOOL initialHealth;
    BOOL initialEnergy;
    BOOL initialCoin;
    BOOL initialAttack;
    BOOL initialDefense;
    
    //Monster
    BOOL initialMonsterHealth;
    
    
    //Player initial values
    NSString *playerStartingLives;
    NSString *playerStartingEnergy;
    NSString *playerStartingCoin;
    NSString *playerStartingHealth;
    NSString *playerStartingAttack;
    NSString *playerStartingDefense;
    
    //Monster initial values
    NSString *monsterStartingHealth;
    
    //public game instance so the view and game can communicate with each other
    Game *gameInstance;
    NSMutableArray *neighbors;
    NSArray *exits;
    Level *l;
    
    //Current values that are delegated to the view controller
    //"passed" variables are passed if the player uses the "cheat" screen
    int currentCell;            BOOL passedAwesomeBoolean;
    int currentPlayerEnergy;    int passedPlayerEnergy;
    int currentPlayerCoin;      int passedPlayerCoin;
    int currentPlayerLives;     int passedPlayerLives;
    int currentPlayerHealth;    int passedPlayerHealth;
    int currentPlayerAttack;    int passedPlayerAttack;
    int currentPlayerDefense;   int passedPlayerDefense;
    int currentMonsterCell;     int passedMonsterHealth;
    int oneRandomItemPosition;  int passedMonsterAttack;
    int twoRandomItemPosition;
    int threeRandomItemPosition;
    int swordItemPosition;
    int coinsItemPosition;
    int leatherArmorItemPosition;
    int bunnyPosition;
    int currentMonsterHealth;
    
    //IMAGES
    UIImage *imagebg; //background image that is constantly overwritten
    
    //borders
    UIColor *red;
    UIImage *black;
    UIImage *black_thin;
    UIImage *highlighted;
    UIImage *highlighted_medium;
    UIImage *rightHighlighted;
    UIImage *rightblack;
    UIImage *leftHighlighted;
    UIImage *leftBlack;
    UIImage *leftWhite;
    UIImage *rightWhite;
    UIImage *white;
    
    //Player UI elements
    UIImage *avatar_ui;
    UIImage *avatarVSushi;
    UIImage *avatarVLime;
    UIImage *coin_ui;
    UIImage *health_ui;
    UIImage *attack_ui;
    UIImage *defense_ui;
    UIImage *lime_ui;
    UIImage *energy;
    
    //Player, Monster, and NPC elements
    UIImage *avatar;
    UIImage *sushi;
    UIImage *lime;
    UIImage *limeRed;
    UIImage *bunny;
    UIImage *bunnyAvatar;
    UIImage *bunnyRed;
    
    //Items and their corresponding items with avatar (ie, when player is in
    //the same cell as an object)
    UIImage *sword;
    UIImage *swordAvatar;
    UIImage *swordRed;
    UIImage *leatherArmor;
    UIImage *leatherArmorAvatar;
    UIImage *leatherArmorRed;
    UIImage *cake;
    UIImage *cakeAvatar;
    UIImage *cakeRed;
    UIImage *donut;
    UIImage *donutAvatar;
    UIImage *donutRed;
    UIImage *coin;
    UIImage *coinAvatar;
    UIImage *coinRed;
    UIImage *coins;
    UIImage *coinsAvatar;
    UIImage *coinsRed;

    //Obstacle elements
    UIImage *upStair;
    UIImage *upStairAvatar;
    UIImage *downStair;
    UIImage *downStairAvatar;
    UIImage *fence;


    //BUTTONS
    //NSMutableArray *buttonArray; //optimization for later: store buttons in
    //an array instead of just x/y coordinates
    UIButton *button;
    UIButton *rightButton;
    UIButton *leftButton;

}

@property NSMutableArray *neighbors;
@property NSMutableArray *buttonArray;
@property int passedPlayerEnergy;
@property int passedPlayerCoin;
@property int passedPlayerLives;
@property int passedPlayerHealth;
@property int passedPlayerAttack;
@property int passedPlayerDefense;
@property int passedMonsterHealth;
@property int passedMonsterAttack;
@property BOOL passedAwesomeBoolean;

//segue for cheat screen
- (IBAction)swirlyGesture:(id)sender;

//segue death screen
-(void)playerIsDead;

//creating UI
//Buttons
-(void)createSideButtons;
-(void)resetSideButtons;
-(void)drawMiddleButtons;

//Player UI
-(void)drawPlayerUI;

//Delegator methods
-(void)modelHasUpdated:(int)currentCell;

//Delegator methods Player information
-(void)modelHasUpdatedPlayerEnergy:(int)playerEnergy;
-(void)modelHasUpdatedPlayerCoin:(int)playerCoin;
-(void)modelHasUpdatedPlayerLives:(int)playerLivesA;
-(void)modelHasUpdatedPlayerHealth:(int)playerHealthA;
-(void)modelHasUpdatedPlayerAttack:(int)playerAttackA;
-(void)modelHasUpdatedPlayerDefense:(int)playerDefenseA;

//Delegator methods Monster information
-(void)modelHasUpdateMonster:(int)monsterLocation;

//Delegator methods item information
-(void)modelHasUpdatedOneRandomItemPostion:(int)oneRandomItemPositionA;
-(void)modelHasUpdatedTwoRandomItemPostion:(int)twoRandomItemPositionA;
-(void)modelHasUpdatedThreeRandomItemPosition:(int)threeRandomItemPositionA;
-(void)modelHasUpdatedSwordItemPosition:(int)swordItemPositionA;
-(void)modelHasUpdatedLeatherArmorItemPosition:(int)leatherArmorItemPositionA;
-(void)modelHasUpdatedCoinsItemPosition:(int)coinsItemPositionA;
-(void)modelHasUpdatedBunnyPosition:(int)bunnyPositionA;
-(void)modelHasUpdatedMonsterHealth:(int)monsterCurrentHealthA;

//Delegator method to force a draw of the screen
-(void)modelHasUpdated;

//Button controls
- (IBAction)leftButton:(id)sender;
- (IBAction)rightButton:(id)sender;
- (IBAction)locationButton:(id)sender;
- (IBAction)loadGameButton:(id)sender;

//programmatically segue to a new view
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;


//setting nil to restart after leaving cheat screen
-(void)restart;

@end
