//
//  View.m
//  2dGameRemake
//
//  Created by Joseph Reiter on 11/11/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "ourCustomViewController.h"
#import <QuartzCore/QuartzCore.h>


@implementation ourCustomViewController

@synthesize neighbors,buttonArray;
@synthesize passedAwesomeBoolean, passedPlayerLives, passedMonsterHealth, passedPlayerEnergy, passedPlayerCoin, passedPlayerAttack, passedPlayerDefense, passedPlayerHealth, passedMonsterAttack;

- (void)viewDidLoad
{
    initialSegue = TRUE;
    didCheat = passedAwesomeBoolean;
    
    initialLives = TRUE;
    initialHealth = TRUE;
    initialEnergy = TRUE;
    initialCoin = TRUE;
    initialAttack = TRUE;
    initialDefense = TRUE;
    
    initialMonsterHealth = TRUE;
    
    playerStartingLives = @"1";
    playerStartingEnergy = @"100";
    playerStartingCoin = @"0";
    playerStartingHealth = @"100";
    playerStartingAttack = @"1";
    playerStartingDefense = @"0";

    monsterStartingHealth = @"10";
    
    imagebg = [UIImage imageNamed:@"black.jpg"];
    [super viewDidLoad];
    
    GameStartMusic *gameStartMusicInstance;
    [gameStartMusicInstance stopGameStartMusic];

    //UI Elements
    avatar = [UIImage imageNamed:@"avatar"];
    avatar_ui = [UIImage imageNamed:@"avatar_ui"];
    coin_ui = [UIImage imageNamed:@"coin_ui"];
    energy = [UIImage imageNamed:@"energy_ui"];
    health_ui = [UIImage imageNamed:@"heart_ui"];
    attack_ui = [UIImage imageNamed:@"crosshair_ui"];
    defense_ui = [UIImage imageNamed:@"defense_ui"];
    lime_ui = [UIImage imageNamed:@"lime_ui"];
    
    //NPC/Monster Elements
    sushi = [UIImage imageNamed:@"sushi"];
    avatarVSushi = [UIImage imageNamed:@"avatarVSushi"];
    lime = [UIImage imageNamed:@"lime"];
    avatarVLime = [UIImage imageNamed:@"avatar_v_Lime"];
    limeRed = [UIImage imageNamed:@"lime_red_border"];
    bunny = [UIImage imageNamed:@"easter_bunny"];
    bunnyAvatar = [UIImage imageNamed:@"easter_bunny_avatar"];
    bunnyRed = [UIImage imageNamed:@"easter_bunny_red_border"];


    //Other object Elements
    cake = [UIImage imageNamed:@"cake_with_border"];
    cakeAvatar = [UIImage imageNamed:@"cake_with_avatar"];
    cakeRed = [UIImage imageNamed:@"cake_with_red_border"];
    donut = [UIImage imageNamed:@"donut"];
    donutAvatar = [UIImage imageNamed:@"donut_Avatar"];
    donutRed = [UIImage imageNamed:@"donut_red_border"];
    coin = [UIImage imageNamed:@"coin"];
    coinAvatar = [UIImage imageNamed:@"coin_avatar"];
    coinRed = [UIImage imageNamed:@"coin_red_border"];
    coins = [UIImage imageNamed:@"coins"];
    coinsAvatar = [UIImage imageNamed:@"coins_avatar"];
    coinsRed = [UIImage imageNamed:@"coins_red_border"];
    sword = [UIImage imageNamed:@"sword"];
    swordAvatar = [UIImage imageNamed:@"sword_avatar"];
    swordRed = [UIImage imageNamed:@"sword_red_border"];
    leatherArmor = [UIImage imageNamed:@"leather_chest_armor"];
    leatherArmorAvatar = [UIImage imageNamed:@"leather_chest_armor_avatar"];
    leatherArmorRed = [UIImage imageNamed:@"leather_chest_armor_red_border"];
    

    //Borders
    highlighted = [UIImage imageNamed:@"highlighted_thin"];
    highlighted_medium = [UIImage imageNamed:@"highlighted_medium"];
    black =[UIImage imageNamed:@"black_thin.png"];
    rightHighlighted = [UIImage imageNamed:@"rightSideButton_highlighted_thin_text"];
    rightblack = [UIImage imageNamed:@"rightSideButton_black_thin_text"];
    leftHighlighted = [UIImage imageNamed:@"leftSideButton_highlighted_thin_text"];
    leftBlack = [UIImage imageNamed:@"leftSideButton_black_thin_text"];
    leftWhite = [UIImage imageNamed:@"leftSideButton_white"];
    rightWhite = [UIImage imageNamed:@"rightSideButton_white"];
    white = [UIImage imageNamed:@"Untitled"];
    black_thin = [UIImage imageNamed:@"black_thin"];
    
    //Static object Elements/obstacles
    upStair = [UIImage imageNamed:@"stairs_up"];
    downStair = [UIImage imageNamed:@"stairs_down"];
    upStairAvatar = [UIImage imageNamed:@"stairs_up_avatar"];
    downStairAvatar = [UIImage imageNamed:@"stairs_down_avatar"];
    fence = [UIImage imageNamed:@"fence"];

    //If they cheated, pass the updated values else start the game with default values
    if(passedAwesomeBoolean == TRUE){
        NSLog(@"boolean is true");
        gameInstance = nil;
        gameInstance = [[Game alloc] initWithPlayerLives:passedPlayerLives andPlayerHealth:passedPlayerHealth andPlayerEnergy:passedPlayerEnergy andPlayerCoin:passedPlayerCoin andPlayerAttack:passedPlayerAttack andPlayerDefense:passedPlayerDefense andMonsterHealth:passedMonsterHealth andMonsterAttack:passedMonsterAttack];
            
    }
    else{
        NSLog(@"NOPE NOPE NOPE");
        gameInstance = [[Game alloc]init];
    }

    //get neighbors and exits to draw
    neighbors = [[NSMutableArray alloc]initWithArray:[gameInstance getNeighbors]];
    exits = [gameInstance getExits];
    [gameInstance setViewUpdateDelegate:self];
    //NSLog(@"passedSomething %i", passedPlayerEnergy);

    //get initial values from game
    currentMonsterCell = gameInstance.monsterStartingPosition;
    oneRandomItemPosition = gameInstance.oneRandomItemLocation;
    twoRandomItemPosition = gameInstance.twoRandomItemLocation;
    threeRandomItemPosition = gameInstance.threeRandomItemLocation;
    swordItemPosition = gameInstance.swordItemLocation;
    leatherArmorItemPosition = gameInstance.leatherArmorItemLocation;
    coinsItemPosition = gameInstance.coinsItemLocation;
    bunnyPosition = gameInstance.bunnyPosition;
    
    //draw the screen
    [self drawMiddleButtons];
    [self createSideButtons];
    [self drawPlayerUI];
    

    //put game initalization in another method close it with a boolean after the first time
	// Do any additional setup after loading the view, typically from a nib.

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//segue to the cheat screen with a rotate gesture
- (IBAction)swirlyGesture:(id)sender {
    NSLog(@"Swirly Gestures!! %d %i", initialSegue, currentCell);
    if (initialSegue && currentCell == 0){
        [self performSegueWithIdentifier:@"CheatCodeView" sender:self];
        initialSegue = FALSE;
    }
    else {
        NSLog(@"Sorry, not initial");
    }
}

//if called, segue to the player is dead view
-(void)playerIsDead{
    playerIsDead = TRUE;
    [self performSegueWithIdentifier:@"playerDeathView" sender:self];
}

//code to create the two side buttons (left and right)
-(void)createSideButtons{
    
    //LEFT BUTTON
    leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.frame = CGRectMake(0,0,76,301);
    [leftButton setTitle:@"" forState:UIControlStateNormal];
    [leftButton setTag:98];
    
    imagebg = leftWhite;
    
    NSNumber *r = [[NSNumber alloc]initWithInt:currentCell];
    if ([exits containsObject:r]){
        if(gameInstance.hierarchy == 0){
            imagebg = leftWhite;
            [leftButton setEnabled:NO];
        }
        if(gameInstance.hierarchy == 1){
            //NSLog(@"will set left to highlighted");
            imagebg = leftHighlighted;
            [leftButton setEnabled:YES];
        }

    }

    [leftButton setBackgroundImage:imagebg forState:UIControlStateNormal];
    [leftButton setBackgroundImage:imagebg forState:UIControlStateSelected];
    [leftButton setBackgroundImage:imagebg forState:UIControlStateDisabled];
    [leftButton addTarget:self action:@selector(leftButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:leftButton];
    
    
    //RIGHT BUTTON
    rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(496,0,73,301);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setTag:99];
    

    imagebg = rightWhite;
    
    if ([exits containsObject:r]){
        if(gameInstance.hierarchy == 0){
            //NSLog(@"will set right to highlighted");
            imagebg = rightHighlighted;
            [rightButton setEnabled:YES];
        }
        if(gameInstance.hierarchy == 1){
            imagebg = rightWhite;
            [rightButton setEnabled:NO];
        }

    }
    [rightButton setBackgroundImage:imagebg forState:UIControlStateNormal];
    [rightButton setBackgroundImage:imagebg forState:UIControlStateSelected];
    [rightButton setBackgroundImage:imagebg forState:UIControlStateDisabled];
    [rightButton addTarget:self action:@selector(rightButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightButton];
    
}
//reset the side button view to correct color border issues. Draw white and then draw what it's
//supposed to be
-(void)resetSideButtons{
    
    //LEFT BUTTON
    leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.frame = CGRectMake(0,0,76,301);
    [leftButton setTitle:@"" forState:UIControlStateNormal];
    [leftButton setTag:98];
    imagebg = leftWhite;
    [leftButton setBackgroundImage:imagebg forState:UIControlStateNormal];
    [leftButton setBackgroundImage:imagebg forState:UIControlStateSelected];
    [leftButton setBackgroundImage:imagebg forState:UIControlStateDisabled];
    [leftButton addTarget:self action:@selector(leftButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:leftButton];
    
    //RIGHT BUTTON
    rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(496,0,76,301);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setTag:99];
    imagebg = rightWhite;
    [rightButton setBackgroundImage:imagebg forState:UIControlStateNormal];
    [rightButton setBackgroundImage:imagebg forState:UIControlStateSelected];
    [rightButton setBackgroundImage:imagebg forState:UIControlStateDisabled];
    [rightButton addTarget:self action:@selector(rightButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightButton];
}

//LEFT BUTTON action
//change level down
- (IBAction)leftButton:(id)sender{
    //NSLog(@"Left button pressed");

    NSNumber *r = [[NSNumber alloc]initWithInt:currentCell];
    if([exits containsObject:r]){
        if (gameInstance.hierarchy == 1){
            gameInstance.hierarchy--;
            //NSLog(@"Hierarchy = %d", gameInstance.hierarchy);
            [gameInstance changeLevel:currentCell];

        }
    }
    [self drawMiddleButtons];
}

//RIGHT BUTTON action
//change level up
- (IBAction)rightButton:(id)sender{

    NSNumber *r = [[NSNumber alloc]initWithInt:currentCell];
    if([exits containsObject:r]){
        if (gameInstance.hierarchy == 0){
            gameInstance.hierarchy++;
            //NSLog(@"Hierarchy = %d", gameInstance.hierarchy);
            [gameInstance changeLevel:currentCell];

        }
    }
    [self drawMiddleButtons];
}

//click any middle button
//go to that location (send tag info to game)
- (IBAction)locationButton:(id)sender{
    int tag = [sender tag];
    [gameInstance buttonClick:tag];
}

-(void)drawMiddleButtons{
    //TODO in later implementations
    //For cellsObjects, delegeate all objects to ViewController instead of using currentLevel == x
    //
    
    int x = 76;
    int i = 0;
    int y = 0;
    

    NSNumber *cId = [NSNumber alloc];
        while (y<242){
        x = 76;
        while (x <= 436) {
            imagebg = black;
            cId = [[NSNumber alloc]initWithInt:i];
            if([neighbors containsObject:cId])
            {
                imagebg = highlighted;
            }
            else {

                imagebg = black;
            }
            if(cId.integerValue == currentCell){
                imagebg = avatar;
            }
            //CAKE
            if(cId.integerValue == oneRandomItemPosition && gameInstance.currentLevel == 0){
                if([neighbors containsObject:cId])
                {
                    imagebg = cakeRed;
                }
                else if(cId.integerValue == currentCell){
                    imagebg = cakeAvatar;
                }
                else {
                    imagebg = white;
                    button = [UIButton buttonWithType:UIButtonTypeCustom];
                    button.frame = CGRectMake(x,y,60,60);
                    [button setTag:i];
                    [button setBackgroundImage:imagebg forState:UIControlStateNormal];
                    [button setBackgroundImage:imagebg forState:UIControlStateSelected];
                    
                    [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:button];
                    imagebg = cake;
                }

            }

            //Objects specific to a certain level
            
            //monster
            if(cId.integerValue == currentMonsterCell && gameInstance.currentLevel == 1){
                if([neighbors containsObject:cId])
                {
                    imagebg = limeRed;
                }
                else if(cId.integerValue == currentCell){
                    imagebg = avatarVLime;
                }
                else {
                    imagebg = white;
                    button = [UIButton buttonWithType:UIButtonTypeCustom];
                    button.frame = CGRectMake(x,y,60,60);
                    [button setTag:i];
                    [button setBackgroundImage:imagebg forState:UIControlStateNormal];
                    [button setBackgroundImage:imagebg forState:UIControlStateSelected];
                    
                    [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:button];
                    NSLog(@"lime");
                    imagebg = lime;
                }
            }

            //bunny
            if(cId.integerValue == bunnyPosition && gameInstance.currentLevel == 0){
                if([neighbors containsObject:cId])
                {
                    imagebg = bunnyRed;
                }
                else if(cId.integerValue == currentCell){
                    imagebg = bunnyAvatar;
                }
                else {
                    imagebg = white;
                    button = [UIButton buttonWithType:UIButtonTypeCustom];
                    button.frame = CGRectMake(x,y,60,60);
                    [button setTag:i];
                    [button setBackgroundImage:imagebg forState:UIControlStateNormal];
                    [button setBackgroundImage:imagebg forState:UIControlStateSelected];
                    
                    [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:button];
                    imagebg = bunny;
                }
            }

            
            //coin
            if(cId.integerValue == twoRandomItemPosition && gameInstance.currentLevel == 1){
                if([neighbors containsObject:cId])
                {
                    imagebg = coinRed;
                }
                else if(cId.integerValue == currentCell){
                    imagebg = coinAvatar;
                }
                else {
                    imagebg = white;
                    button = [UIButton buttonWithType:UIButtonTypeCustom];
                    button.frame = CGRectMake(x,y,60,60);
                    [button setTag:i];
                    [button setBackgroundImage:imagebg forState:UIControlStateNormal];
                    [button setBackgroundImage:imagebg forState:UIControlStateSelected];
                    
                    [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:button];
                    imagebg = coin;
                }
            }

            //coins
            if(cId.integerValue == coinsItemPosition && gameInstance.currentLevel == 0){
                if([neighbors containsObject:cId])
                {
                    imagebg = coinsRed;
                }
                else if(cId.integerValue == currentCell){
                    imagebg = coinsAvatar;
                }
                else {
                    imagebg = white;
                    button = [UIButton buttonWithType:UIButtonTypeCustom];
                    button.frame = CGRectMake(x,y,60,60);
                    [button setTag:i];
                    [button setBackgroundImage:imagebg forState:UIControlStateNormal];
                    [button setBackgroundImage:imagebg forState:UIControlStateSelected];
                    
                    [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:button];
                    imagebg = coins;
                }
            }

            //SWORD
            if(cId.integerValue == swordItemPosition && gameInstance.currentLevel == 0){
                if([neighbors containsObject:cId])
                {
                    imagebg = swordRed;
                }
                else if(cId.integerValue == currentCell){
                    imagebg = swordAvatar;
                }
                else {
                    imagebg = white;
                    button = [UIButton buttonWithType:UIButtonTypeCustom];
                    button.frame = CGRectMake(x,y,60,60);
                    [button setTag:i];
                    [button setBackgroundImage:imagebg forState:UIControlStateNormal];
                    [button setBackgroundImage:imagebg forState:UIControlStateSelected];
                    
                    [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:button];
                    imagebg = sword;
                }
            }

            //LEATHER ARMOR
            if(cId.integerValue == leatherArmorItemPosition && gameInstance.currentLevel == 1){
                if([neighbors containsObject:cId])
                {
                    imagebg = leatherArmorRed;
                }
                else if(cId.integerValue == currentCell){
                    imagebg = leatherArmorAvatar;
                }
                else {
                    imagebg = white;
                    button = [UIButton buttonWithType:UIButtonTypeCustom];
                    button.frame = CGRectMake(x,y,60,60);
                    [button setTag:i];
                    [button setBackgroundImage:imagebg forState:UIControlStateNormal];
                    [button setBackgroundImage:imagebg forState:UIControlStateSelected];
                    
                    [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:button];
                    imagebg = leatherArmor;
                }
            }

            
            //donuts
            if(cId.integerValue == threeRandomItemPosition && gameInstance.currentLevel == 1){
                if([neighbors containsObject:cId])
                {
                    imagebg = donutRed;
                }
                else if(cId.integerValue == currentCell){
                    imagebg = donutAvatar;
                }
                else {
                    imagebg = white;
                    button = [UIButton buttonWithType:UIButtonTypeCustom];
                    button.frame = CGRectMake(x,y,60,60);
                    [button setTag:i];
                    [button setBackgroundImage:imagebg forState:UIControlStateNormal];
                    [button setBackgroundImage:imagebg forState:UIControlStateSelected];
                    
                    [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:button];
                    imagebg = donut;
                }
            }

            
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(x,y,60,60);
            [button setTag:i];
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];

            [self.view addSubview:button];
            
            
            i++;
            x+=60;

            }
        y+=60;

    }
    if (gameInstance.currentLevel == 0){
        
        //Objects specific to certain levels

        
        //STAIR ICONS
        if (currentCell == 20){
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(436,120,60,60);
            [button setTag:21];
            imagebg = black;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
            
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(436,120,60,60);
            [button setTag:20];
            imagebg = upStairAvatar;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
        }
        else {
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(436,120,60,60);
            [button setTag:20];
            imagebg = upStair;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
        }
        if (currentCell == 21){
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(76,180,60,60);
            [button setTag:21];
            imagebg = black;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
            
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(76,180,60,60);
            [button setTag:21];
            imagebg = upStairAvatar;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
        }
        else {
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(76,180,60,60);
            [button setTag:21];
            imagebg = upStair;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
        }

        
        //FENCE ICONS
        x = 76;
        y = 240;
        int tag = 28;
        while (x<=436){ //28-34
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(x,y,60,60);
            [button setTag:tag];
            imagebg = fence;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            [button setBackgroundImage:imagebg forState:UIControlStateDisabled];
            [button setEnabled:NO];

            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
            x+=60;
            tag++;
        }
    }
    
    if (gameInstance.currentLevel == 1){
        
        if (currentCell == 20){
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(436,120,60,60);
            [button setTag:21];
            imagebg = black;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
            
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(436,120,60,60);
            [button setTag:20];
            imagebg = downStairAvatar;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
        }
        else {
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(436,120,60,60);
            [button setTag:20];
            imagebg = downStair;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
        }
        if (currentCell == 21){
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(76,180,60,60);
            [button setTag:21];
            imagebg = black;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
            
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(76,180,60,60);
            [button setTag:21];
            imagebg = downStairAvatar;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
        }
        else {            
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(76,180,60,60);
            [button setTag:21];
            imagebg = downStair;
            [button setBackgroundImage:imagebg forState:UIControlStateNormal];
            [button setBackgroundImage:imagebg forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(locationButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
        }
    }
    
}

-(void)drawPlayerUI{
    //PLAYER AVATAR
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(3,4,21,21)];
    imageView.image = avatar_ui;
    [self.view addSubview:imageView];
    
    UILabel *livesLabelView = [[UILabel alloc] initWithFrame:CGRectMake(26, 4, 30, 13)];
    livesLabelView.font = [UIFont fontWithName:@"Times New Roman" size:12];
    
    NSString *livesText = [NSString stringWithFormat:@"%d",currentPlayerLives];
    if (initialLives == TRUE){
        livesLabelView.text = playerStartingLives;
        initialLives = FALSE;
    }
    else{
        livesLabelView.text = livesText;
    }
    [self.view addSubview:livesLabelView];
    
    //MONSTER AVATAR
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(540, 4, 21, 21)];
    imageView.image = lime_ui;
    [self.view addSubview:imageView];
    
    //MONSTER HEALTH
    UILabel *monsterHealthLabelView = [[UILabel alloc] initWithFrame:CGRectMake(540, 26, 30, 13)];
    monsterHealthLabelView.font = [UIFont fontWithName:@"Times New Roman" size:12];
    
    NSString *monsterHealthText = [NSString stringWithFormat:@"%d",currentMonsterHealth];
    NSLog(@"current monster health %i", currentMonsterHealth);
    if (initialMonsterHealth == TRUE){
        monsterHealthLabelView.text = monsterStartingHealth;
        NSLog(@"starting monster health %@", monsterStartingHealth);
        initialMonsterHealth = FALSE;
    }
    else{
        monsterHealthLabelView.text = monsterHealthText;
    }
    [self.view addSubview:monsterHealthLabelView];
    
    
    //HEALTH
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(3,26,11,11)];
    imageView.image = health_ui;
    [self.view addSubview:imageView];
    
    UILabel *healthLabelView = [[UILabel alloc] initWithFrame:CGRectMake(18, 26, 30, 13)];
    healthLabelView.font = [UIFont fontWithName:@"Times New Roman" size:12];
    
    NSString *healthText = [NSString stringWithFormat:@"%d",currentPlayerHealth];
    if (initialHealth == TRUE){
        healthLabelView.text = playerStartingHealth;
        initialHealth = FALSE;
    }
    else{
        healthLabelView.text = healthText;
    }
    //NSLog(@"currentPlayerHealth: %d", currentPlayerHealth);
    [self.view addSubview:healthLabelView];
    
    //ENERGY
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(3,39,11,11)];
    imageView.image = energy;
    [self.view addSubview:imageView];
    
    UILabel *energyLabelView = [[UILabel alloc] initWithFrame:CGRectMake(18, 39, 31, 13)];
    energyLabelView.font = [UIFont fontWithName:@"Times New Roman" size:12];
    NSString *energyText = [NSString stringWithFormat:@"%d",currentPlayerEnergy];
    if (initialEnergy == TRUE){
        energyLabelView.text = playerStartingEnergy;
        initialEnergy = FALSE;
    }
    else{
        energyLabelView.text = energyText;
    }
    [self.view addSubview:energyLabelView];
    
    
    //COIN
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(3,52,11,11)];
    imageView.image = coin_ui;
    [self.view addSubview:imageView];
    
    UILabel *coinLabelView = [[UILabel alloc] initWithFrame:CGRectMake(18, 52, 31, 13)];
    coinLabelView.font = [UIFont fontWithName:@"Times New Roman" size:12];

    NSString *coinText = [NSString stringWithFormat:@"%d",currentPlayerCoin];
    if (initialCoin == TRUE){
        coinLabelView.text = playerStartingCoin;
        initialCoin = FALSE;
    }
    else{
        coinLabelView.text = coinText;
    }

    [self.view addSubview:coinLabelView];
    
    //ATTACK
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(3,67,11,11)];
    imageView.image = attack_ui;
    [self.view addSubview:imageView];
    
    UILabel *attackLabelView = [[UILabel alloc] initWithFrame:CGRectMake(18, 67, 31, 13)];
    attackLabelView.font = [UIFont fontWithName:@"Times New Roman" size:12];
    
    NSString *attackText = [NSString stringWithFormat:@"%d",currentPlayerAttack];
    if (initialAttack == TRUE){
        attackLabelView.text = playerStartingAttack;
        initialAttack = FALSE;
    }
    else{
        attackLabelView.text = attackText;
    }
    //NSLog(@"currentPlayerAttack: %d", currentPlayerAttack);
    [self.view addSubview:attackLabelView];

    
    //DEFENSE
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(3,82,11,11)];
    imageView.image = defense_ui;
    [self.view addSubview:imageView];
    
    UILabel *defenseLabelView = [[UILabel alloc] initWithFrame:CGRectMake(18, 82, 31, 13)];
    defenseLabelView.font = [UIFont fontWithName:@"Times New Roman" size:12];
    
    NSString *defenseText = [NSString stringWithFormat:@"%d",currentPlayerDefense];
    if (initialDefense == TRUE){
        defenseLabelView.text = playerStartingDefense;
        initialDefense = FALSE;
    }
    else{
        defenseLabelView.text = defenseText;
    }
    //NSLog(@"currentPLayerDefense: %d", currentPlayerDefense);
    [self.view addSubview:defenseLabelView];
}

/*method to call when the view is out of date
 this method is delegated from game.
 */
-(void)modelHasUpdated:(int)currentCellarg{
    neighbors = [gameInstance.levelContainer[gameInstance.currentLevel] getNeighbors];//change 0 to current level when there are multipl
    currentCell = currentCellarg;
    //NSLog(@"current cell in viewcontroller = %i", currentCell);
    [self drawMiddleButtons];
    [self resetSideButtons];
    [self createSideButtons];
    [self drawPlayerUI];
}

-(void)modelHasUpdatedPlayerEnergy:(int)playerEnergy{
    NSLog(@"<CustomViewController> UPDATING!!!");
    currentPlayerEnergy = playerEnergy;
    [self drawPlayerUI];
}
-(void)modelHasUpdatedPlayerCoin:(int)playerCoinA{
    NSLog(@"<CustomViewController> UPDATING!!!");
    currentPlayerCoin = playerCoinA;
    [self drawPlayerUI];
}

-(void)modelHasUpdatedPlayerLives:(int)playerLivesA{
    NSLog(@"<CustomViewController> UPDATING!!!");
    currentPlayerLives = playerLivesA;
    [self drawPlayerUI];
}
-(void)modelHasUpdatedPlayerHealth:(int)playerHealthA{
    NSLog(@"<CustomViewController> UPDATING!!!");
    currentPlayerHealth = playerHealthA;
    [self drawPlayerUI];
}
-(void)modelHasUpdatedPlayerAttack:(int)playerAttackA{
    NSLog(@"<CustomViewController> UPDATING!!!");
    currentPlayerAttack = playerAttackA;
    [self drawPlayerUI];
}
-(void)modelHasUpdatedPlayerDefense:(int)playerDefenseA{
    NSLog(@"<CustomViewController> UPDATING!!!");
    currentPlayerDefense = playerDefenseA;
    [self drawPlayerUI];
}

-(void)modelHasUpdateMonster:(int)monsterLocation{
    currentMonsterCell = monsterLocation;
    [self drawMiddleButtons];
    
}
-(void)modelHasUpdatedOneRandomItemPostion:(int)oneRandomItemPositionA{
    oneRandomItemPosition = oneRandomItemPositionA;
}
-(void)modelHasUpdatedTwoRandomItemPostion:(int)twoRandomItemPositionA{
    twoRandomItemPosition = twoRandomItemPositionA;
}
-(void)modelHasUpdatedThreeRandomItemPosition:(int)threeRandomItemPositionA{
    threeRandomItemPosition = threeRandomItemPositionA;
}
-(void)modelHasUpdatedSwordItemPosition:(int)swordItemPositionA{
    swordItemPosition = swordItemPositionA;
}
-(void)modelHasUpdatedLeatherArmorItemPosition:(int)leatherArmorItemPositionA{
    leatherArmorItemPosition = leatherArmorItemPositionA;
}
-(void)modelHasUpdatedCoinsItemPosition:(int)coinsItemPositionA{
    coinsItemPosition = coinsItemPositionA;
}
-(void)modelHasUpdatedBunnyPosition:(int)bunnyPositionA{
    bunnyPosition = bunnyPositionA;
    bunnyClicked = TRUE;
    [self performSegueWithIdentifier:@"bunnyNPCTalk" sender:self];
}
-(void)modelHasUpdatedMonsterHealth:(int)monsterCurrentHealthA{
    NSLog(@"<VIEWCONTROLLER> modelHasUpdatedMonstersHealth");
    currentMonsterHealth = monsterCurrentHealthA;
    [self drawPlayerUI];
}
-(void)modelHasUpdated{
    [self drawMiddleButtons];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (playerIsDead){
        NSLog(@"DO NOTHING");
        //playerDeathView *pdv = (playerDeathView *)[segue destinationViewController];
        
    }
    else if (playerWins){
        NSLog(@"DO NOTHING");
        //youWinView *yw = (youWinView *)[segue destinationViewController];
    }
    else if (bunnyClicked){
        NSLog(@"DO NOTHING");
        //bunnyNPCTalk *bnpc = (bunnyNPCTalk *)[segue destinationViewController];
    }
    else{
        CheatCodeView *vc = (CheatCodeView *)[segue destinationViewController];
        NSLog(@"<OURCUSTOMVIEW> Passing game instance");
        vc.passedGameInstance = gameInstance;
    }

}

-(void)playerWinsGame{
    playerWins = TRUE;
    [self performSegueWithIdentifier:@"youWinView" sender:self];
}

-(void)restart{
    gameInstance = nil;
    //didCheat = TRUE;
}

@end
