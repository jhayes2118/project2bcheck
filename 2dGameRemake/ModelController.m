//
//  ViewController.m
//  2dGameRemake
//
//  Created by Joseph Reiter on 11/11/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "ModelController.h"


@interface ModelController ()

@end


@implementation ModelController


- (void)viewDidLoad
{
    gameStartMusicInstance = [[GameStartMusic alloc]init];
    [gameStartMusicInstance playGameStartMusic];
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loadGameButton:(id)sender {
    //NSLog(@"loadGameButton clicked");
    [gameStartMusicInstance stopGameStartMusic];

}

- (IBAction)giantButton:(id)sender{
    //NSLog(@"Giant button clicked");
    [gameStartMusicInstance stopGameStartMusic];

}


@end
