//
//  ViewController.h
//  2dGameRemake
//
//  Created by Joseph Reiter on 11/11/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GameStartMusic.h"
#import "Game.h"
#import "player.h"

@interface ModelController : UIViewController {

    GameStartMusic *gameStartMusicInstance;
    Game *gameInstance;
    int currentCell;
    int currentLevel;
}

- (IBAction)leftButton:(id)sender;
- (IBAction)rightButton:(id)sender;
- (IBAction)locationButton:(id)sender;
- (IBAction)loadGameButton:(id)sender;
- (IBAction)giantButton:(id)sender;


@end
