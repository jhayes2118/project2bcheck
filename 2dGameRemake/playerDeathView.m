//
//  playerDeathView.m
//  2dGameRemake
//
//  Created by Joseph Reiter on 12/8/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "playerDeathView.h"

@interface playerDeathView ()

@end

@implementation playerDeathView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
