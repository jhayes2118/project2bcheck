//
//  MonsterTests.m
//  2dGameRemake
//
//  Created by John Hayess on 12/10/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "MonsterTests.h"

@implementation MonsterTests
-(void)setUp{
    p = [[Player alloc]initWithDescription:@"player1" AndInteractable:YES];
    c = [[Cell alloc]initWithId:1];
    m = [[Monster alloc]init];
    [c addMonster:m];
    [c addPlayer:p];
}

-(void)tearDown{

}
-(void)testChangeLocation{
    int i = c.cellsMonster.location;
    [c.cellsMonster changeLocation:13];
    STAssertEquals(13, c.cellsMonster.location, @"the location is not correct");
    STAssertFalse(c.cellsMonster.location == i, @"the locations are the same and they shouldn't be");
    
     
}
-(void)testGetMonsterHealth{
    [c.cellsMonster setHealth:15];
    int i = c.cellsMonster.health;
    STAssertEquals(15, i, @"the health is not correct");
    
    
}
-(void)testIncrementMonsterHealth{
    [c.cellsMonster setHealth:15];
    int i = c.cellsMonster.health;
    [c.cellsMonster incrementMonsterHealth:5];
    STAssertEquals(i+5, c.cellsMonster.health, @"the health is not correct");
}

-(void)testAttack{
    int h = [c.playerObject getPlayerHealth];
    int a = c.cellsMonster.attackPower;
    [c.cellsMonster attack];
    int h1 = [c.playerObject getPlayerHealth];
    STAssertEquals(h-a, h1, @"the healths are not correct");
}
-(void)testReceiveAttack{
    int h = c.cellsMonster.health;
    int a = [c.playerObject getPlayerAttack];
    [c.playerObject sendAttack];
    int h1 = c.cellsMonster.health;
    STAssertEquals(h-a, h1, @"the healths are not correct");
}


@end
