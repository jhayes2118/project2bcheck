//
//  modelControllerTests.h
//  2dGameRemake
//
//  Created by Joseph Reiter on 11/11/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "SmallObject.h"
#import "Cell.h"
#import "Level.h"
#import "Player.h"

@interface modelControllerTests : SenTestCase{
    
}
-(void)testCurrentLevelDelegator;
-(void)testCurrentLocationDelegator;

@property Player *s;
@property Level *l1;
@property Level *l2;


@end
