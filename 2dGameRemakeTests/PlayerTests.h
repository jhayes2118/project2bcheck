//
//  PlayerTests.h
//  2dGameRemake
//
//  Created by John Hayess on 12/10/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "Player.h"
#import "Cell.h"
#import "Monster.h"

@interface PlayerTests : SenTestCase{

    Player *p;
    Player *p2;
    Cell *c;
    Monster *m;
    
}

@property Player *p;
@property Player *p2;
@property Cell *c;
@property Monster *m;


-(void) changeLocationTest;
-(void)setDelegateTest;
-(void)testEnergy;
-(void)testCoin;
-(void)testLives;
-(void)testHealth;
-(void)testAttack;
-(void)testRecieveAttack;
-(void)testSendAttack;
-(void)testDefense;


@end
