//
//  PlayerTests.m
//  2dGameRemake
//
//  Created by John Hayess on 12/10/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "PlayerTests.h"

@implementation PlayerTests
@synthesize p ,p2;
-(void)setUp{
    p = [[Player alloc]initWithDescription:@"player1" AndInteractable:YES];
    p2 = [[Player alloc]initWithDescription:@"player2" AndInteractable:YES];
    c = [[Cell alloc]initWithId:1];
    m = [[Monster alloc]init];
    [c addMonster:m];
}
-(void)tearDown{
    
}
-(void) changeLocationTest{
    
    [p changeLocation:3];
    STAssertEquals(3, p.location, @"the location is not correct");
    [p changeLocation:4];
    STAssertEquals(4, p.location, @"the location is not correct on reset");
    
}

-(void)testEnergy{
        int e = [p getPlayerEnergy];
    [p incrementPlayerEnergy:-3];
    STAssertEquals(e-3, [p getPlayerEnergy], @"the energy is not correct");
    
}
-(void)testCoin{
    int c = [p getPlayerCoin];
    [p incrementPlayerCoin:3];
    STAssertEquals(c+3, [p getPlayerCoin], @"the coin number is not correct");
}
-(void)testLives{
    int c = [p getPlayerLives];
    [p incrementLives:3];
    STAssertEquals(c+3, [p getPlayerLives], @"the Lives number is not correct");
}
-(void)testHealth{
    int c = [p getPlayerHealth];
    [p incrementHealth:3];
    STAssertEquals(c+3, [p getPlayerHealth], @"the Health number is not correct");
}
-(void)testRecieveAttack{
    int h = [p getPlayerHealth];
    int r = m.attackPower;
    [p setFightDelegate:c];
    [c addPlayer:p];
    [c.cellsMonster attack];
    p = c.playerObject;
    STAssertEquals(h-r, [p getPlayerHealth], @"The health is incorrect");
    
    
    
}
-(void)testSendAttack{  Monster *m1 = c.cellsMonster;
    int health = c.cellsMonster.health;
    int a = [p getPlayerAttack];
    [p setFightDelegate:c];
    [c addPlayer:p];
    [p sendAttack];
    m1 = c.cellsMonster;
    int healthafter = m1.health;
    STAssertEquals(health-a, healthafter, @"the healths are not correct");}

@end
