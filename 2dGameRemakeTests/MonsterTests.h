//
//  MonsterTests.h
//  2dGameRemake
//
//  Created by John Hayess on 12/10/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "Monster.h"
#import "Cell.h"
#import "Player.h"


@interface MonsterTests : SenTestCase
{
    Monster *m;
    Cell *c;
    Player *p;
    
}

@property Monster *m;
@property Cell *c;
@property Player *p;

-(void)testChangeLocation;
-(void)testGetMonsterEnergy;
-(void)testAttack;
-(void)testReceiveAttack;
-(void)testIncrementMonsterHealth;
@end
