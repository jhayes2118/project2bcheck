//
//  levelTests.h
//  2dGame
//
//  Created by John Hayess on 11/10/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "SmallObject.h"
#import "Cell.h"
#import "Level.h"

@interface levelTests : SenTestCase{
    


}
-(void)testAddNeighbors;
-(void)testMoveObject;
-(void)testRecieveObject;
-(void)testClearCells;
-(void)testGetPlayer;

@property Cell *c;
@property SmallObject *s;
@property SmallObject *s2;
@property Level *l1;
@property Level *l2;

@end
