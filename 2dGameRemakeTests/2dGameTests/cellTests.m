//
//  _dGameTests.m
//  2dGameTests
//
//  Created by Joseph Reiter on 11/4/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "cellTests.h"



@implementation cellTests
@synthesize c,s,s2;

- (void)setUp
{
    [super setUp];
     s = [[SmallObject alloc] init];
     s2 = [[SmallObject alloc] init];
     c  = [[Cell alloc] initWithId:1 AndObject:s];

    
}

- (void)tearDown
{
    
    
    s = nil;
    s2 = nil;
    c = nil;
    [super tearDown];
    
}


-(void)testisEmpty{
    [c remove];
    [c remove];
    STAssertTrue(c.empty, @"it isn't empty");
    [c addObject:s];
    STAssertFalse(c.empty, @"it is empty");
    
    [c addObject:s];
    [c addObject:s2];
    
    
    
}

-(void)testGetId{
    
    STAssertEquals(1, c.cId, @"they are not equal");
    NSLog(@"the id is %d",c.cId );
}
-(void)testAddObject{
    [c remove];
    STAssertTrue(c.empty, @"It is not empty");
    
    [c addObject:s];
    STAssertFalse([c empty], @"It is not empty");
    STAssertEquals(s, c.cellsObject, @"they are not equal");
}
-(void)testRemove{
    [c remove];
    STAssertTrue(c.empty, @"it is not empty");
    
}
//tests adding neighbors
-(void)testLocationAdd{
    int location = 1;
    [c addNeighbor:location];
    bool a = [c containsNeighbor:location];
    STAssertTrue(a, @"the neighbor doesn't exist");
    
    
}
-(void)testLocationsAddAll{
    int arr[2] = { 2, 3 };
}
@end
