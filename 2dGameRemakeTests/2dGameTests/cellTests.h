//
//  _dGameTests.h
//  2dGameTests
//
//  Created by Joseph Reiter on 11/4/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "Cell.h"
#import "SmallObject.h"

@interface cellTests : SenTestCase

-(void)setUp;
-(void)tearDown;
-(void)testisEmpty;
-(void)testContains;
-(void)testGetId;
-(void)testAddObject;
-(void)testRemove;
-(void)testLocationAdd;
-(void)testLocationsAddAll;
-(void)testLocationsRemove;
-(void)testGetLocations;


@property Cell *c;
@property SmallObject *s;
@property SmallObject *s2;

@end
