//
//  CheatCodeView.m
//  2dGameRemake
//
//  Created by Joseph Reiter on 12/7/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "CheatCodeView.h"


@interface CheatCodeView ()

@end

@implementation CheatCodeView
@synthesize passedGameInstance;
@synthesize playerLivesLabel, playerHealthLabel, playerEnergyLabel, playerCoinLabel, playerAttackLabel, playerDefenseLabel;
@synthesize monsterHealthLabel, monsterAttackLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    reset = FALSE;
    
    livesTouched = FALSE;
    healthTouched = FALSE;
    energyTouched = FALSE;
    coinTouched = FALSE;
    attackTouched = FALSE;
    defenseTouched = FALSE;
    mHealthTouched = FALSE;
    mAttackTouched = FALSE;
    
    defaultPlayerLives = passedGameInstance.playerStartingLives;
    defaultPlayerHealth = passedGameInstance.playerStartingHealth;
    defaultPlayerEnergy = passedGameInstance.playerStartingEnergy;
    defaultPlayerAttack = passedGameInstance.playerStartingAttack;
    defaultPlayerDefense = passedGameInstance.playerStartingDefense;
    defaultMonsterHealth = 10;
    defaultMonsterAttack = 10;
    heDidCheat = TRUE;
    
    [self updateLabels];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)playerLivesStepper:(UIStepper *)sender {
    newPlayerLives = [sender value];
    intToText = [NSString stringWithFormat:@"%d",newPlayerLives];
    playerLivesLabel.text = intToText;
    livesTouched = TRUE;
}

- (IBAction)playerAttackStepper:(UIStepper *)sender {
    newPlayerAttack = [sender value];
    intToText = [NSString stringWithFormat:@"%d",newPlayerAttack];
    playerAttackLabel.text = intToText;
    attackTouched = TRUE;
    
}

- (IBAction)playerHealthStepper:(UIStepper *)sender {
    newPlayerHealth = [sender value];
    intToText = [NSString stringWithFormat:@"%d",newPlayerHealth];
    playerHealthLabel.text = intToText;
    healthTouched = TRUE;
}

- (IBAction)playerEnergyStepper:(UIStepper *)sender {
    newPlayerEnergy = [sender value];
    intToText = [NSString stringWithFormat:@"%d",newPlayerEnergy];
    playerEnergyLabel.text = intToText;
    energyTouched = TRUE;
}

- (IBAction)playerCoinStepper:(UIStepper *)sender {
    newPlayerCoin = [sender value];
    intToText = [NSString stringWithFormat:@"%d", newPlayerCoin];
    playerCoinLabel.text = intToText;
    coinTouched = TRUE;
}

- (IBAction)playerDefenseStepper:(UIStepper *)sender {
    newPlayerDefense = [sender value];
    intToText = [NSString stringWithFormat:@"%d",newPlayerDefense];
    playerDefenseLabel.text = intToText;
    defenseTouched = TRUE;
}

- (IBAction)monsterHealthStepper:(UIStepper *)sender {
    newMonsterHealth = [sender value];
    intToText = [NSString stringWithFormat:@"%d",newMonsterHealth];
    monsterHealthLabel.text = intToText;
    mHealthTouched = TRUE;
}

- (IBAction)monsterAttackStepper:(UIStepper *)sender {
    newMonsterAttack = [sender value];
    intToText = [NSString stringWithFormat:@"%d",newMonsterAttack];
    monsterAttackLabel.text = intToText;
    mAttackTouched = TRUE;
}

- (IBAction)backButton:(id)sender {
}

- (IBAction)resetButton:(id)sender {
    
    newPlayerLives = defaultPlayerLives;
    newPlayerHealth = defaultMonsterHealth;
    newPlayerEnergy = defaultPlayerEnergy;
    newPlayerCoin = defaultPlayerCoin;
    newPlayerAttack = defaultPlayerAttack;
    newPlayerDefense = defaultPlayerDefense;
    newMonsterHealth = defaultMonsterHealth;
    newMonsterAttack = defaultMonsterAttack;
    
    [self updateLabels];
    reset = TRUE;
}

-(void)updateLabels{
    intToText = [NSString stringWithFormat:@"%d",defaultPlayerLives];
    playerLivesLabel.text = intToText;
    
    intToText = [NSString stringWithFormat:@"%d",defaultPlayerHealth];
    playerHealthLabel.text = intToText;
    
    intToText = [NSString stringWithFormat:@"%d",defaultPlayerEnergy];
    playerEnergyLabel.text = intToText;
    
    intToText = [NSString stringWithFormat:@"%d",defaultPlayerAttack];
    playerAttackLabel.text = intToText;
    
    intToText = [NSString stringWithFormat:@"%d",defaultPlayerDefense];
    playerDefenseLabel.text = intToText;
    
    intToText = [NSString stringWithFormat:@"%d",defaultMonsterHealth];
    monsterHealthLabel.text = intToText;
    
    intToText = [NSString stringWithFormat:@"%d",defaultMonsterAttack];
    monsterAttackLabel.text = intToText;
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ourCustomViewController *vc = (ourCustomViewController *)[segue destinationViewController];
    NSLog(@"<OURCUSTOMVIEW> Passing game instance");
    vc.passedAwesomeBoolean = heDidCheat;
    
    vc.passedPlayerLives = defaultPlayerLives;
    vc.passedPlayerHealth = defaultPlayerHealth;
    vc.passedPlayerEnergy = defaultPlayerEnergy;
    vc.passedPlayerCoin = defaultPlayerCoin;
    vc.passedPlayerAttack = defaultPlayerAttack;
    vc.passedPlayerDefense = defaultPlayerDefense;
    vc.passedMonsterHealth = defaultMonsterHealth;
    vc.passedMonsterAttack = defaultMonsterAttack;
    
    if (livesTouched){
        vc.passedPlayerLives = newPlayerLives;
    }
    if (healthTouched){
        vc.passedPlayerHealth = newPlayerHealth;
    }
    if (energyTouched){
        vc.passedPlayerEnergy = newPlayerEnergy;
    }
    if (coinTouched){
        vc.passedPlayerCoin = newPlayerCoin;
    }
    if (attackTouched){
        vc.passedPlayerAttack = newPlayerAttack;
    }
    if (defenseTouched){
        vc.passedPlayerDefense = newPlayerDefense;
    }
    if (mHealthTouched){
        vc.passedMonsterHealth = newMonsterHealth;
    }
    if (mAttackTouched){
        vc.passedMonsterAttack = newMonsterAttack;
    }
    [vc restart];
    passedGameInstance = nil;
    vc = nil;
}


@end
