//
//  GameStartMusic.m
//  2dGameRemake
//
//  Created by Joseph Reiter on 11/19/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "GameStartMusic.h"

@implementation GameStartMusic
@synthesize soundFilePath, soundFileURL, player;

-(id)init{
    self = [super init];    
    if(self)
    {
        return self;
    }
    else {return NULL;}
}

-(void)playGameStartMusic{
    soundFilePath = [[NSBundle mainBundle] pathForResource:@"unicorn" ofType:@"mp3"];
    soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = -1; //infinite
    [player play];
}
-(void)stopGameStartMusic{
 [player stop];
 player = nil;
 }

@end
