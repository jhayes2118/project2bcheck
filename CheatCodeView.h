//
//  CheatCodeView.h
//  2dGameRemake
//
//  Created by Joseph Reiter on 12/7/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ourCustomViewController.h"
#import "Game.h"
#import "Player.h"
#import "Monster.h"
#import "Level.h"
#import "Cell.h"

@interface CheatCodeView : UIViewController{
    NSString *intToText;
    BOOL heDidCheat;
    BOOL reset;
    
    int defaultPlayerLives;
    int defaultPlayerHealth;
    int defaultPlayerEnergy;
    int defaultPlayerCoin;
    int defaultPlayerAttack;
    int defaultPlayerDefense;
    int defaultMonsterHealth;
    int defaultMonsterAttack;
    
    int newPlayerLives;     BOOL livesTouched;
    int newPlayerHealth;    BOOL healthTouched;
    int newPlayerEnergy;    BOOL energyTouched;
    int newPlayerCoin;      BOOL coinTouched;
    int newPlayerAttack;    BOOL attackTouched;
    int newPlayerDefense;   BOOL defenseTouched;
    int newMonsterHealth;   BOOL mHealthTouched;
    int newMonsterAttack;   BOOL mAttackTouched;
}
@property (weak, nonatomic) IBOutlet UILabel *playerLivesLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerHealthLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerEnergyLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerCoinLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerAttackLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerDefenseLabel;
@property (weak, nonatomic) IBOutlet UILabel *monsterHealthLabel;
@property (weak, nonatomic) IBOutlet UILabel *monsterAttackLabel;
@property Game *passedGameInstance;

- (IBAction)playerLivesStepper:(UIStepper *)sender;
- (IBAction)playerAttackStepper:(UIStepper *)sender;
- (IBAction)playerHealthStepper:(UIStepper *)sender;
- (IBAction)playerEnergyStepper:(UIStepper *)sender;
- (IBAction)playerCoinStepper:(UIStepper *)sender;
- (IBAction)playerDefenseStepper:(UIStepper *)sender;
- (IBAction)monsterHealthStepper:(UIStepper *)sender;
- (IBAction)monsterAttackStepper:(UIStepper *)sender;

- (IBAction)backButton:(id)sender;
- (IBAction)resetButton:(id)sender;
- (IBAction)saveButton:(id)sender;
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

-(void)updateLabels;

@end
