//
//  Cell.h
//  2dGame
//
//  Created by Joseph Reiter on 11/4/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SmallObject.h"
#import "Player.h"
#import "Monster.h"

@interface Cell : NSObject{
    
    int cId;
    SmallObject *cellsObject;
    Player *playerObject;
    Monster *cellsMonster;
    NSMutableArray *neighbors;
    int movementEnergyCost;
    BOOL hasPlayer;
    BOOL fight;
    int numberOfNeighbors;
    
}

@property SmallObject *cellsObject;
@property Player *playerObject;
@property Monster *cellsMonster;
@property int cId;
@property int numberOfNeighbors;
@property BOOL empty;
@property BOOL hasMonster;
@property NSMutableArray *neighbors;
@property BOOL hasPlayer;
@property BOOL fight;




- (id)initWithId:(int)ident;
- (id)initWithId:(int)ident AndObject:(SmallObject*)theObject;
- (id)initWithId:(int)ident AndNeighbors:(NSArray*)theNeighbors;
- (id)initWithId:(int)ident AndNeighbors:(NSArray*)theNeighbors AndObject:(SmallObject*)theObject;

-(BOOL)empty;
-(int)getId;
-(void)addObject:(SmallObject *)newObject;
-(void)addObject:(SmallObject *)newObject andWhereToPut:(int)to;
-(void)addPlayer:(Player *)newPlayer;
-(void)addPlayer:(Player *)newPlayer andWhereToPut:(int)to;
-(void)addMonster:(Monster*)theMonster;
-(void)addMonster:(Monster*)theMonster andWhereToPut:(int)to;

-(BOOL)cellHasMonster;

-(SmallObject *)remove;
-(Player *)removePlayer;
-(Monster*)removeMonster;

-(void)addNeighbor:(int)aNeighbor;
-(BOOL)containsNeighbor:(int)aNeighbor;
-(int)getRandomNeighbor;

-(BOOL)containsSmallObject:(SmallObject*)theObject;
-(NSMutableArray*)getNeighbors;

-(void)startFight;
-(void)stopFight;
-(void)sendAttack:(int)attackValue;

-(void)playerAttack;
-(void)monsterDie;

@end
