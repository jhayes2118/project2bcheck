//
//  Player.h
//  2dGameRemake
//
//  Created by Joseph Reiter on 11/11/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "SmallObject.h"

@protocol PlayerLocationDelegate<NSObject>
    -(void)locationHasChangedTo:(int)number;

@end
@protocol PlayerEnergyDelegate<NSObject>
    -(void)energyHasChangedTo:(int)energyNumber;
    -(void)coinHasChangedTo:(int)coinNumber;
    -(void)livesHasChangedTo:(int)livesNumber;
    -(void)healthHasChangedTo:(int)healthNumber;
    -(void)attackHasChangedTo:(int)attackNumber;
    -(void)defenseHasChangedTo:(int)defenseNumber;
    -(void)playerIsInTheBeyond;
@end
@protocol FightDelegate <NSObject>
-(void) playerSendAttack:(int)attackValue;
@end

@interface Player : SmallObject
{
    int location;
    id <PlayerLocationDelegate>locDelegate;
    int playerEnergy;
    id <PlayerEnergyDelegate>eneDelegate;
    id<FightDelegate>fightDelegate;
    int playerCoin;
    BOOL fighting;
    int playerLives;
    int playerHealth;
    int playerAttack;
    int playerDefense;
    
}

@property int location;
@property BOOL fighting;
@property int playerLives;
@property int playerHealth;
@property int playerAttack;
@property int playerDefense;
@property (nonatomic,retain) id locDelegate;
@property (nonatomic,retain) id eneDelegate;
@property (nonatomic,retain) id FightDelegate;

-(id)init;
-(void)changeLocation:(int)newLocation;

-(void)incrementPlayerEnergy:(int)setNewEnergy;
-(int)getPlayerEnergy;

-(void)incrementPlayerCoin:(int)setNewCoin;
-(int)getPlayerCoin;

-(void)incrementLives:(int)setNewLives;
-(int)getPlayerLives;

-(void)incrementHealth:(int)setNewHealth;
-(int)getPlayerHealth;

-(void)incrementAttack:(int)setNewAttack;
-(int)getPlayerAttack;

-(void)incrementDefense:(int)setNewDefense;
-(int)getPlayerDefense;

-(void)recieveAttack:(int)attackValue;
-(void)sendAttack;
@end
