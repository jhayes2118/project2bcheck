//
//  Monster.h
//  2dGameRemake
//
//  Created by John Hayess on 11/29/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SmallObject.h"

@protocol MonsterLocationDelegate<NSObject>
-(void)monsterLocationHasChangedTo:(int)number;
-(void)monsterKilled;
-(void)monsterHealthHasChangedTo:(int)health;
@end
@protocol FightDelegate <NSObject>
-(void)monsterSendAttack:(int)attackValue;
-(void)monsterDie;
@end


@interface Monster : SmallObject
{
    int location;
    NSString *iAMAMonster;
    id <MonsterLocationDelegate>monLocDelegate;
    id<FightDelegate>monFightDelegate;
    int energy;
    BOOL fighting;
    NSTimer *timer;
    NSThread *timerThread;
    int health;
    int attackPower;
    
}

@property int location;
@property (nonatomic,retain) id monLocDelegate;
@property (nonatomic,retain) id eneDelegate;
@property BOOL fighting;
@property  int health;
@property  int attackPower;

-(id)initWithDelegate:(id)theDelegate;
-(id)initWithDescription:(NSString *)newDescription andDelegate:(id)theDelegate;
-(void)changeLocation:(int)newLocation;
-(void)changeEnergy:(int)newEnergy;
-(int)getMonsterEnergy;

-(void)move;
-(void)attack;
-(void)setLocationDelegate:(id)newDelegate;
-(void)setFightDelegate:(id)newDelegate;
-(void)createThread;

-(void)recieveAttack:(int)attackValue;

-(void)incrementMonsterHealth:(int)newHealth;

@end
