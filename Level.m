//
//  Level.m
//  2dGame
//
//  Created by John Hayess on 11/9/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import "Level.h"

@implementation Level

SystemSoundID mBeep; //used for making the beep
@synthesize lId,description,currentCell,cells, levelDelegator;
@synthesize movementEnergyCost;

/*
 Initalize with exit and entrance relationship
 */
-(id)initWithId:(int)ident andWithExitCells:(NSArray*)exitsCells andWithExitLevels:(NSArray*)exitLevels{
    self = [super init];
    cells = [[NSMutableArray alloc]init];

    for (int i = 0 ; i < 36; i++){
        cells[i] = [[Cell alloc] initWithId:(i)];
    }
    
    exits = [[NSMutableDictionary alloc]initWithObjects:exitLevels forKeys:exitsCells];
    currentCell = 0;
    monsterLocation = 23;
    movementEnergyCost = -1;
    monsterNextToPlayer=FALSE;

    return self;
}
/*
 adds neighbors to a cell
 */
-(void)addNeighbors:(int[])neighbors tocId:(int)cId withNumberOf:(int)n{
    for(int i = 0; i < n ; i++){
        [cells[cId] addNeighbor:neighbors[i]];
    }
}
/*
 adds a neighbor to a cell
 */
-(void)addSingleNeighbor:(int)neighbor tocId:(int)cId{
    [cells [cId] addNeighbor:cId];
    }

/*
 THIS SHOULD BE CALLED MOVE PLAYER This method moves the player to the new cell
 
 @param to the cells to move the player to
 */

-(void)moveObject:(int)to {
    if(![cells[currentCell] fight]){
        if([cells[currentCell] containsNeighbor:to]){
            NSLog(@"<LEVEL> MOVING <PLAYER>");
            Player *inTransit = [cells[currentCell] removePlayer];
            NSLog(@"<LEVEL> HAS <PLAYER>");

            //NSLog(@"All values, lives, health, attack defense %i, %i, %i, %i", [inTransit getPlayerLives], [inTransit getPlayerHealth], [inTransit getPlayerAttack], [inTransit getPlayerDefense]);
            //NSLog(@"After removing player oldCeLL 1 == isempty -> %d",[cells[currentCell] isEmpty]);
            [cells[to] addPlayer:inTransit];
            //NSLog(@"player moved from %d to %d",currentCell, to);
            currentCell = to;
            //NSLog(@"<LEVEL> {moveObject} changeEergy");
            [inTransit incrementPlayerEnergy:movementEnergyCost];
            [levelDelegator levelHasChanged:currentCell];

            }
        else{
            [self playBeep];
        }
    }
else{
    [self playBeep];
    }
}

/*
 adds a player to the level
 
 used in initaliztion of the game to pass the player to the cell
 
 used by game to take the player from one level to another 
 */
-(void)addPlayer:(Player*)thePlayer{
    [thePlayer setLocDelegate:self];
    [thePlayer setEneDelegate:self];
    [cells[currentCell] addPlayer:thePlayer];//change to new method adding to proper cell
                                            //this calls "Cells" addPlayer call, this is correct
}
/*
 adds a player to the level
 
 used in initaliztion of the game to pass the player to the cell
 
 used by game to take the player from one level to another
 
 @param to which cell to put the player in
 */
-(void)addPlayer:(Player*)thePlayer andWhereToPut:(int)to{
    [thePlayer setLocDelegate:self];
    [thePlayer setEneDelegate:self];
    currentCell = to;
    [cells[currentCell] addPlayer:thePlayer];//paramaterize and add player to desired cell?
                                            //this calls "Cells" addPlayer call, this is correct as
    
}
/*
 adds a monster to the level
 
 used in initaliztion of the game to pass the player to the cell

 */
-(void)addMonster:(Monster *)theMonster{
    //set delegates (if needed)
    
    //NSLog(@"<LEVEL> Set delegate");
    [theMonster setMonLocDelegate:self];
    [cells[currentCell] addMonster:theMonster];
}
/*
 adds a monster to the level
 
 used in initaliztion of the game to pass the player to the cell
 
 used by game to put the monster in a specific place */
-(void)addMonster:(Monster*)theMonster andWhereToPut:(int)to{
    //set delegates (if needed)
    [theMonster setMonLocDelegate:self];
    monsterLocation = to;
    [cells[to] addMonster:theMonster];
}

/*
 adds an object to a specified cell in the level
 */
-(void)addObject:(SmallObject *)newObject andWhereToPut:(int)to{
    [cells[to] addObject:newObject];
}

/*
 returns the neighbors of the current cell
 used for the UI to display the neighbors 
 */
-(NSMutableArray*)getNeighbors{
    return [cells[currentCell] getNeighbors];
    
}

/*
 returns the levels exits by cell 
 @return array keys a list of the exts
 */

-(NSArray*)getExits{
    return [exits allKeys];
}

/*
 Returns any exits to other levels for the current cell
 */

-(NSNumber*)getExit:(int)currentCelli{//may not need the current cell paramater passed since it already knows it
    NSNumber *n = [[NSNumber alloc]initWithInt:currentCelli];
    return [exits objectForKey:n];
}

/*
 This is a delegated method the current cell has been changed by a player being added
 to a cell
 
 the game delegate method is called to update the current cell in game
 */

-(void)locationHasChangedTo:(int)number{
    currentCell = number;
    if(monsterLocation > 0){
    BOOL temp = [cells[monsterLocation] hasMonster];
    NSLog(@"monsterlocation has monster?: %d", temp);
    
    
    int tempNum = [cells[monsterLocation] getId];
    NSLog(@"Here is your monster location: %i and your player cell: %i", tempNum, currentCell);
    
    if ([cells[monsterLocation] getId] == currentCell){
        NSLog(@"I win");
    }
    
    if ([cells[currentCell] hasMonster]) {
        NSLog(@"You cannot move, you're under attack");
        [cells[currentCell] startFight];
    }
    }
     
    else {
 
    }
    
    [levelDelegator levelHasChanged:currentCell];
}
/*
This is a delegated method from cell to level 
 This is called when a monster is added to a cell
 
 The game delegate method is called ot update the current monster location in the game
 */
-(void)monsterLocationHasChangedTo:(int)number{
    if(number > 0){
        tempMonster = [cells[monsterLocation] removeMonster];
         NSLog(@"<LEVEL> [mosterLocationHasChangedTo DELEGATE] Entered");
        if(monsterNextToPlayer){
            monsterLocation = monsterNextMove;
            monsterNextToPlayer = FALSE;
        }
       
        
        else{
            if(0 < number){
            int r = [cells[monsterLocation] getRandomNeighbor];
                monsterLocation = r;
            }
        }
        [cells[monsterLocation] addMonster:tempMonster];
        [levelDelegator monsterHasMoved:monsterLocation];
        //BOOL temp = [cells[monsterLocation] hasMonster];
        //NSLog(@"Temp: %d", temp);
        
        //int tempNum = [cells[monsterLocation] getId];
        //NSLog(@"Here is your tempNum sir: %i and your tempCall: %i", tempNum, currentCell);
        if([cells[monsterLocation] containsNeighbor:currentCell]){
                NSLog(@"warn player here");
                monsterNextToPlayer = TRUE;
                monsterNextMove = currentCell;
            }
        if ([cells[currentCell] hasMonster]) {
            NSLog(@"You cannot move, you're attacking the helpless thing");
            [cells[currentCell] startFight];
        }
    }
    else{
        monsterLocation = -1;
        [levelDelegator monsterHasMoved:monsterLocation];
    }
}

/*
The next block of code is to update all of the variables for player and monster to the game to be passed to the view controller to be displayed in the view
 */
   
-(void)monsterHealthHasChangedTo:(int)health{
    NSLog(@"<LEVEL> monsterHealthHasChanged %i", health);
    [levelDelegator monstersHealthHasChanged:health];
}

-(void)energyHasChangedTo:(int)energyNumber{
    NSLog(@"<LEVEL> EnergyHasChangedTo %d", energyNumber);
    [levelDelegator energyHasChanged:energyNumber];
}

-(void)coinHasChangedTo:(int)coinNumber{
    NSLog(@"<LEVEL> coinHasChangedTo %d", coinNumber);
    [levelDelegator coinHasChanged:coinNumber];
}


-(void)livesHasChangedTo:(int)livesNumber{
    NSLog(@"<LEVEL> livesHasChangedTo %d", livesNumber);
    [levelDelegator livesHasChanged:livesNumber];
}

-(void)healthHasChangedTo:(int)healthNumber{
    NSLog(@"<LEVEL> healthHasChangedTo %d", healthNumber);
    [levelDelegator healthHasChanged:healthNumber];
}

-(void)attackHasChangedTo:(int)attackNumber{
    NSLog(@"<LEVEL> attackHasChangedTo %d", attackNumber);
    [levelDelegator attackHasChanged:attackNumber];
}

-(void)defenseHasChangedTo:(int)defenseNumber{
    NSLog(@"<LEVEL> defenseHasChangedTo %d", defenseNumber);
    [levelDelegator defenseHasChanged:defenseNumber];
}

/*
Sets the delegate of the level. Used to link the game with the level to pass variables to be displayed
 */
- (void)setDelegate:(id)newDelegate {
    levelDelegator = newDelegate;
}
/*
 This informs the game that the player is dead and the game should end
 */
-(void)playerIsInTheBeyond{
    [levelDelegator playerIsInTheBeyond];
}

/*
This method destroys all cells in the level
 */
-(void)clearCells{
    cells = nil;
}

/*
Plays a beep 
 */
- (void) playBeep
{
    // Create the sound ID and play the sound
    
    NSString* path = [[NSBundle mainBundle]
                      pathForResource:@"beep" ofType:@"wav"];
    NSURL* url = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &mBeep);
    
    AudioServicesPlaySystemSound(mBeep);
    
}
/*
 gets the player from the current cell and returns it
 */
-(Player*)getPlayer{
    return [cells[currentCell] removePlayer];
}

/*
 informs the delegate that the monster has died
 */
-(void)monsterKilled{
    [levelDelegator monsterKilled];
}

@end
