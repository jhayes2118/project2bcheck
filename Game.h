//
//  Game.h
//  2dGame
//
//  Created by Joseph Reiter on 11/4/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import  <Foundation/Foundation.h>
#import "Level.h"
#import  "Cell.h"
#import "SmallObject.h"
#import "Monster.h"

@protocol ViewUpdate<NSObject>
-(void)playerIsDead;
-(void)modelHasUpdated:(int)currentCell;
-(void)modelHasUpdatedPlayerEnergy:(int)playerEnergy;
-(void)modelHasUpdatedPlayerCoin:(int)playerCoin;
-(void)modelHasUpdatedPlayerLives:(int)playerLives;
-(void)modelHasUpdatedPlayerHealth:(int)playerHealth;
-(void)modelHasUpdatedPlayerAttack:(int)playerAttack;
-(void)modelHasUpdatedPlayerDefense:(int)playerDefense;
-(void)modelHasUpdateMonster:(int)monsterLocation;
-(void)modelHasUpdatedOneRandomItemPostion:(int)oneRandomItemPosition;
-(void)modelHasUpdatedTwoRandomItemPostion:(int)twoRandomItemPosition;
-(void)modelHasUpdatedThreeRandomItemPosition:(int)threeRandomItemPosition;
-(void)modelHasUpdatedSwordItemPosition:(int)swordItemPosition;
-(void)modelHasUpdatedLeatherArmorItemPosition:(int)leatherArmorItemPosition;
-(void)modelHasUpdatedCoinsItemPosition:(int)coinsItemPosition;
-(void)modelHasUpdatedBunnyPosition:(int)bunnyPosition;
-(void)modelHasUpdatedMonsterHealth:(int)monsterCurrentHealth;
-(void)playerWinsGame;
-(void)modelHasUpdated;
@end


@interface Game : NSObject <LevelContentsDelegator>{

    BOOL initialBool;
    
    NSString *titleString;
    NSString *blankString;
    NSString *monsterString;
    NSString *cakeString;
    NSString *donutString;
    NSString *coin1String;
    NSString *coin2String;
    NSString *swordString;
    NSString *bunnyString;
    NSString *leatherArmorString;
    NSMutableArray *levelContainer;
    Level *l1;
    Level *l2;
    Player *tempPlayer;
    Monster *tempMonster;
    int currentLevel;//
    int currentCell;// game shouldn't know the current cell? should we remove? 
    int hierarchy; //-1 lower, 0 main, 1 top
    int playerStartingEnergy;
    int playerStartingLives;
    int playerStartingHealth;
    int playerStartingCoin;
    int playerStartingAttack;
    int playerStartingDefense;
    
    int monsterStartingHealth;
    int monsterStartingAttack;
    
    int monsterStartingPosition;
    int oneRandomItemLocation;
    int twoRandomItemLocation;
    int threeRandomItemLocation;
    int swordItemLocation;
    int leatherArmorItemLocation;
    int coinsItemLocation;
    int bunnyPosition;
}


@property NSString *title;
@property NSArray *levelContainer;
@property int currentLevel;
@property int hierarchy;
@property int monsterStartingPosition;
@property int oneRandomItemLocation;
@property int twoRandomItemLocation;
@property int threeRandomItemLocation;
@property int swordItemLocation;
@property int leatherArmorItemLocation;
@property int coinsItemLocation;
@property int bunnyPosition;
@property (nonatomic,strong) id <ViewUpdate> viewUpdateDelegate;
@property int playerStartingLives;
@property int playerStartingHealth;
@property int playerStartingEnergy;
@property int playerStartingAttack;
@property int playerStartingDefense;

- (id)init;
- (id)initWithPlayerLives:(int)initPlayerLives andPlayerHealth:(int)initPlayerHealth andPlayerEnergy:(int)initPlayerEnergy andPlayerCoin:(int)initPlayerCoin andPlayerAttack:(int)initPlayerAttack andPlayerDefense:(int)initPlayerDefense andMonsterHealth:(int)initMonsterHealth andMonsterAttack:(int)initMonsterAttack;
- (void)createLevels;
 -(void)playerIsInTheBeyond;
- (void)levelHasChanged:(int)number;
- (void)energyHasChanged:(int)currentEnergy;
- (void)coinHasChanged:(int)currentCoin;


-(void)livesHasChanged:(int)currentLives;
-(void)healthHasChanged:(int)currentHealth;
-(void)attackHasChanged:(int)currentAttack;
-(void)defenseHasChanged:(int)currentDefense;

-(void)monsterHasMoved:(int)monsterLocation;//for monster
-(void)monstersHealthHasChanged:(int)currentMonsterHealth;

- (void)createNeighbors;
- (NSArray*)getNeighbors;
- (NSArray*)getExits;

- (void)buttonClick:(int)tag;
- (void)changeLevel:(int)currentCell;
- (void)monsterKilled;

@end
