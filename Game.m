//
//  Game.m
//  2dGame
//
//  Created by Joseph Reiter on 11/4/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//
// work on putting play initialization in level


#import "Game.h"


@implementation Game{
}
@synthesize title,levelContainer,currentLevel,viewUpdateDelegate;
@synthesize hierarchy, monsterStartingPosition, oneRandomItemLocation, twoRandomItemLocation, threeRandomItemLocation, swordItemLocation, leatherArmorItemLocation, coinsItemLocation, bunnyPosition;
@synthesize playerStartingLives, playerStartingAttack, playerStartingDefense, playerStartingEnergy, playerStartingHealth;


/*
 initializes all the variables used in a game to their default values
 */
-(id)init{
    self = [super init];
    if(self){
        NSLog(@"<GAME> You're in the default init meathod");
        initialBool = TRUE;
        
        titleString = @"here is the tile yo";
        blankString = @"blank";
        monsterString = @"MONSTER";
        bunnyString = @"BUNNYNPC";
        cakeString = @"CAKE";
        donutString = @"DONUT";
        coin1String = @"COIN";
        coin2String = @"COINS";
        swordString = @"SWORD";
        leatherArmorString = @"LEATHER ARMOR";
        levelContainer = [[NSMutableArray alloc]initWithCapacity:5];
        currentCell = 0;
        currentLevel = 0;
        hierarchy = 0;
        
        //PLAYER
        playerStartingEnergy = 100;
        playerStartingLives = 1;
        playerStartingHealth = 100;
        playerStartingCoin = 0;
        playerStartingAttack = 1;
        playerStartingDefense = 0;
        
        //MONSTER
        monsterStartingPosition = 23;
        
        //NPC
        bunnyPosition = 24;
        
        //ITEMS
        oneRandomItemLocation = 11;
        twoRandomItemLocation = 24;
        threeRandomItemLocation = 18;
        swordItemLocation = 8;
        leatherArmorItemLocation = 14;
        coinsItemLocation = 27;

        [self createLevels];

    }
    
    if(self)
    {
    return self;
    }
    else {return NULL;}
}
/*
 inizializes a game to specific values
 used for the cheat menu
 */
-(id)initWithPlayerLives:(int)initPlayerLives andPlayerHealth:(int)initPlayerHealth andPlayerEnergy:(int)initPlayerEnergy andPlayerCoin:(int)initPlayerCoin andPlayerAttack:(int)initPlayerAttack andPlayerDefense:(int)initPlayerDefense andMonsterHealth:(int)initMonsterHealth andMonsterAttack:(int)initMonsterAttack{
    NSLog(@"<GAME> you're in the cheat INIT method");
    self = [self init];
    playerStartingLives = initPlayerLives;
    playerStartingHealth = initPlayerHealth;
    playerStartingEnergy = initPlayerEnergy;
    playerStartingCoin = initPlayerCoin;
    playerStartingAttack = initPlayerAttack;
    playerStartingDefense = initPlayerDefense;
    
    monsterStartingHealth = initMonsterHealth;
    monsterStartingAttack = initMonsterAttack;
    
    return self; 
}
/*
 creates the levels that we have set up
 
 adds player and monster to the levels 
 
 puts the objects for the game in the level and cells appropriately 
 */
-(void)createLevels{
    //NSLog(@"Start creating levels");
    NSNumber *l1exit1 = [[NSNumber alloc]initWithInt:20];
    NSNumber *l1exit2 = [[NSNumber alloc]initWithInt:21];
    
    NSNumber *l1exitLevel1 = [[NSNumber alloc]initWithInt:1];
    NSNumber *l1exitLevel2 = [[NSNumber alloc]initWithInt:1];
    
    
    NSArray *l1exits = [[NSArray alloc]initWithObjects: l1exit1, l1exit2, nil];
    NSArray *l1exitLevels = [[NSArray alloc]initWithObjects: l1exitLevel1, l1exitLevel2, nil];
    
    NSNumber *l2exit1 = [[NSNumber alloc]initWithInt:20];
    NSNumber *l2exit2 = [[NSNumber alloc]initWithInt:21];
    
    NSNumber *l2exitLevel1 = [[NSNumber alloc]initWithInt:0];
    NSNumber *l2exitLevel2 = [[NSNumber alloc]initWithInt:0];
    
    
    NSArray *l2exits = [[NSArray alloc]initWithObjects: l2exit1, l2exit2, nil];
    NSArray *l2exitLevels = [[NSArray alloc]initWithObjects: l2exitLevel1, l2exitLevel2, nil];

    
    
    l1 = [[Level alloc]initWithId:0 andWithExitCells:l1exits andWithExitLevels:l1exitLevels];
    [l1 setDelegate:self];
    
     l2 = [[Level alloc]initWithId:1 andWithExitCells:l2exits andWithExitLevels:l2exitLevels];
     [l2 setDelegate:self];
    
    [levelContainer addObject:l1];
    [levelContainer addObject:l2];
    
    //Player *player = [[Player alloc]initWithDescription:@"this is the player" AndInteractable:TRUE];
        //remove interacable players are always interactable
    //[player setPlayerEnergy:playerStartingEnergy];
    
    tempPlayer = [[Player alloc]initWithDescription:@"this is the player" AndInteractable:TRUE];
   
    
    tempMonster = [[Monster alloc]initWithDelegate:self];
    //Monster *monsterLime = [[Monster alloc] initwithDelegate:self];
    //Monster *monster1 = [[Monster alloc] initWithDescription:monsterString andDelegate:self];
    
    SmallObject *bunny = [[SmallObject alloc] initWithDescription:bunnyString AndInteractable:TRUE];
    
    SmallObject *cake = [[SmallObject alloc] initWithDescription:cakeString AndInteractable:TRUE];
    SmallObject *donut = [[SmallObject alloc] initWithDescription:donutString AndInteractable:TRUE];
    SmallObject *coin = [[SmallObject alloc] initWithDescription:coin1String AndInteractable:TRUE];
    SmallObject *coins = [[SmallObject alloc] initWithDescription:coin2String AndInteractable:TRUE];
    SmallObject *sword = [[SmallObject alloc] initWithDescription:swordString AndInteractable:TRUE];
    SmallObject *leatherArmor = [[SmallObject alloc] initWithDescription:leatherArmorString AndInteractable:TRUE];

    [levelContainer[0] addPlayer:tempPlayer];
    
    
    [levelContainer[1] addMonster:tempMonster andWhereToPut:monsterStartingPosition];
    [levelContainer[0] addObject:bunny andWhereToPut:bunnyPosition];
    [levelContainer[0] addObject:cake andWhereToPut:oneRandomItemLocation];
    [levelContainer[0] addObject:sword andWhereToPut:swordItemLocation];
    [levelContainer[0] addObject:coins andWhereToPut:coinsItemLocation];
    [levelContainer[1] addObject:leatherArmor andWhereToPut:leatherArmorItemLocation];
    [levelContainer[1] addObject:coin andWhereToPut:twoRandomItemLocation];
    [levelContainer[1] addObject:donut andWhereToPut:threeRandomItemLocation];
    
    [self createNeighbors];
    
}
/*sets the view delegate for sending model values to the view
 
 */
-(void)setViewUpdateDelegate:(id)delegate{
    viewUpdateDelegate = delegate;
}
/*
The following are the delegate methods to update the various variables to the view
 */
-(void)levelHasChanged:(int)number{
    currentCell = number;
    [viewUpdateDelegate modelHasUpdated:currentCell];
}
//call this method when the player dies
-(void)playerIsInTheBeyond{
    [viewUpdateDelegate playerIsDead];
}
-(void)monsterKilled{
    [viewUpdateDelegate playerWinsGame];
}
-(void)energyHasChanged:(int)currentEnergy{
    [viewUpdateDelegate modelHasUpdatedPlayerEnergy:currentEnergy];
}
-(void)coinHasChanged:(int)currentCoin{
    [viewUpdateDelegate modelHasUpdatedPlayerCoin:currentCoin];
}

-(void)monsterHasMoved:(int)monsterLocation{
    [viewUpdateDelegate modelHasUpdateMonster:monsterLocation];
}

-(void)livesHasChanged:(int)currentLives{
    NSLog(@"<GAME> livesHasChanged");
    [viewUpdateDelegate modelHasUpdatedPlayerLives:currentLives];
}

-(void)healthHasChanged:(int)currentHealth{
    NSLog(@"<GAME> healthHasChanged");
    [viewUpdateDelegate modelHasUpdatedPlayerHealth:currentHealth];
}

-(void)attackHasChanged:(int)currentAttack{
    NSLog(@"<GAME> attackHasChanged");
    [viewUpdateDelegate modelHasUpdatedPlayerAttack:currentAttack];
}

-(void)defenseHasChanged:(int)currentDefense{
    NSLog(@"<GAME> defenseHasChanged");
    [viewUpdateDelegate modelHasUpdatedPlayerDefense:currentDefense];
}

-(void)monstersHealthHasChanged:(int)currentMonsterHealth{
    NSLog(@"<GAME> monsterHealthHasChanged %i", currentMonsterHealth);
    [viewUpdateDelegate modelHasUpdatedMonsterHealth:currentMonsterHealth];
}

/*This handles the button click event
 
 */

-(void)buttonClick:(int)tag{
    NSLog(@"You have clicked the button");
    if (initialBool){
        NSLog(@"lives, health, energy, coin, attack, defense %i %i %i %i %i %i", playerStartingLives, playerStartingHealth, playerStartingEnergy, playerStartingCoin, playerStartingAttack, playerStartingDefense);
        [tempPlayer incrementLives:playerStartingLives];
        [tempPlayer incrementHealth:playerStartingHealth];
        [tempPlayer incrementPlayerEnergy:playerStartingEnergy];
        [tempPlayer incrementPlayerCoin:playerStartingCoin];
        [tempPlayer incrementAttack:playerStartingAttack];
        [tempPlayer incrementDefense:playerStartingDefense];
        [tempMonster incrementMonsterHealth:monsterStartingHealth];
        //NSArray *tempNeighbors = [self getNeighbors];
        //[tempPlayer incrementPlayerEnergy:playerStartingEnergy];
        //NSNumber *tempNumber = [[NSNumber alloc]initWithInt:tag];
        /* if(![tempNeighbors containsObject:tempNumber]){
         }*/
        initialBool = FALSE;
    }
    
      if (tag == currentCell){
        //OPEN MENU
        //OR WHTAEVER
        
          Level *tempLevelContainer = levelContainer[currentLevel];
          Cell *tempCell = tempLevelContainer.cells[tag];
          
          if(tempCell.hasMonster){
             
              NSLog(@"ATTAAAAAAACK!!!! trying to send fight");
              [tempCell playerAttack];

          }
          else{
          //SmallObject *tempPlayer = tempCell.playerObject;
          SmallObject *tempObj = tempCell.cellsObject;
          NSString *tempObjString = tempObj.description;
          
          NSLog(@"<GAME>tempCellString %@", tempObjString);
        //BUNNY
        if ([tempObjString isEqualToString:bunnyString]){
            NSLog(@"<GAME>BUNNY!");
            [viewUpdateDelegate modelHasUpdatedBunnyPosition:bunnyPosition];
            //[viewUpdateDelegate modelHasUpdated];
        }
          //CAKE
          if ([tempObjString isEqualToString:cakeString]){
              NSLog(@"<GAME>YAY CAKE!");
              [tempPlayer incrementPlayerEnergy:50];
              //[levelContainer[currentLevel] cells[tag]]
              [viewUpdateDelegate modelHasUpdatedOneRandomItemPostion:-1];
              [viewUpdateDelegate modelHasUpdated];
              [tempLevelContainer.cells[tag] remove];
          }
          //COIN
          if ([tempObjString isEqualToString:coin1String]) {
              NSLog(@"<GAME>YAY MONEYZZZZ");
              //int tempCoinNumber = [tempPlayer getPlayerCoin] + 1;
              [tempPlayer incrementPlayerCoin:1];
              //[viewUpdateDelegate modelHasUpdatedPlayerCoin:tempCoinNumber];
              [viewUpdateDelegate modelHasUpdatedTwoRandomItemPostion:-1];
              [viewUpdateDelegate modelHasUpdated];
              [tempLevelContainer.cells[tag] remove];
          }
          //COINS
            if ([tempObjString isEqualToString:coin2String]) {
                NSLog(@"<GAME>YAY MONEYZZZZ (MULTIPLE)");
                [tempPlayer incrementPlayerCoin:15];
                [viewUpdateDelegate modelHasUpdatedCoinsItemPosition:-1];
                [viewUpdateDelegate modelHasUpdated];
                [tempLevelContainer.cells[tag] remove];
            }
          //SWORD
              if ([tempObjString isEqualToString:swordString]) {
                  NSLog(@"<GAME>SWORD OF POWAAAA");
                  [tempPlayer incrementAttack:8];
                  [viewUpdateDelegate modelHasUpdatedSwordItemPosition:-1];
                  [viewUpdateDelegate modelHasUpdated];
                  [tempLevelContainer.cells[tag] remove];
              }
          //ARMOR
              if ([tempObjString isEqualToString:leatherArmorString]) {
                  NSLog(@"<GAME>ARMOR UP!");
                  [tempPlayer incrementDefense:3];
                  [viewUpdateDelegate modelHasUpdatedLeatherArmorItemPosition:-1];
                  [viewUpdateDelegate modelHasUpdated];
                  [tempLevelContainer.cells[tag] remove];
              }
          //DONUTS
          if ([tempObjString isEqualToString:donutString]){
              NSLog(@"<GAME>YAY DONUTTZUZUZUZUZ!");
              [tempPlayer incrementPlayerEnergy:20];
              [viewUpdateDelegate modelHasUpdatedThreeRandomItemPosition:-1];
              [viewUpdateDelegate modelHasUpdated];
              [tempLevelContainer.cells[tag] remove];
          }
          else if ([tempObjString isEqualToString:blankString]){
              NSLog(@"<GAME>Nothing to see here....");
          }
          else{
              NSLog(@"<GAME>No object here...");
          }
      }
    }
    else{
    [levelContainer[currentLevel] moveObject:tag];
       }
   }

-(void)changeLevel:(int)currentCella{
    //NSLog(@"enter change level");
    
    NSNumber *n =[levelContainer[currentLevel] getExit:currentCella];//get the exit level number
    Player *temp = [levelContainer[currentLevel] getPlayer];
    
    
    //currentLevel = n.intValue;
    [levelContainer[n.intValue] addPlayer:temp andWhereToPut:currentCell];
    [viewUpdateDelegate modelHasUpdated:currentCell];
    currentLevel = n.intValue;
    NSLog(@"<GAME> currentLevel: %d", currentLevel);
    
}
/*
 returns the neighbors of the current cell
 */
-(NSArray*)getNeighbors{
    
    return  [levelContainer[currentLevel] getNeighbors];
}

/*
 returns the current cell's exits to other levels
 */
-(NSArray*)getExits{
    //NSArray *a =  [levelContainer[currentLevel] getExits];
    return [levelContainer[currentLevel] getExits];
}
//creates all of the neighbor relationships of the game (inital)
-(void)createNeighbors{
    
    int arr[12] = {1,2,3,4,5,6,7,8,9,10,11,12};
    int arr1[5] = {0,3,10,5,13};
    
    [levelContainer[0] addNeighbors:arr tocId:0 withNumberOf:12];
    [levelContainer[0] addNeighbors:arr1 tocId:1 withNumberOf:5];
    
    int ar0[2] = {1, 7};	//pos 0
    int ar1[3] = {0, 2, 8}; //pos 1
    int ar2[3] = {1, 3, 9}; //pos 2
    int ar3[3] = {2, 4, 10}; //pos 3
    int ar4[3] = {3, 5, 11}; //pos 4
    int ar5[3] = {4, 6, 12}; //pos 5
    int ar6[2] = {5, 13}; //pos 6
    
    [levelContainer[0] addNeighbors:ar0 tocId:0 withNumberOf:2];
    [levelContainer[0] addNeighbors:ar1 tocId:1 withNumberOf:3];
    [levelContainer[0] addNeighbors:ar2 tocId:2 withNumberOf:3];
    [levelContainer[0] addNeighbors:ar3 tocId:3 withNumberOf:3];
    [levelContainer[0] addNeighbors:ar4 tocId:4 withNumberOf:3];
    [levelContainer[0] addNeighbors:ar5 tocId:5 withNumberOf:3];
    [levelContainer[0] addNeighbors:ar6 tocId:6 withNumberOf:2];
    
    int ar7[3] = {0, 8, 14}; //pos 7
    int ar8[4] = {1, 7, 9 ,15};	//pos 8
    int ar9[4] = {2, 8, 10, 16}; //pos 9
    int ar10[4] = {3, 9, 11, 17}; //pos 10
    int ar11[4] = {4, 10, 12, 18}; //pos 11
    int ar12[4] = {5, 11, 13, 19}; //pos 12
    int ar13[3] = {6, 12, 20}; //pos 13
    
    [levelContainer[0] addNeighbors:ar7 tocId:7 withNumberOf:3];
    [levelContainer[0] addNeighbors:ar8 tocId:8 withNumberOf:4];
    [levelContainer[0] addNeighbors:ar9 tocId:9 withNumberOf:4];
    [levelContainer[0] addNeighbors:ar10 tocId:10 withNumberOf:4];
    [levelContainer[0] addNeighbors:ar11 tocId:11 withNumberOf:4];
    [levelContainer[0] addNeighbors:ar12 tocId:12 withNumberOf:4];
    [levelContainer[0] addNeighbors:ar13 tocId:13 withNumberOf:3];
    
    int ar14[3] = {7, 15, 21}; //pos 14
    int ar15[4] = {8, 14, 16, 22}; //pos 15
    int ar16[4] = {9, 15, 17, 23}; //pos 16
    int ar17[4] = {10, 16, 18, 24}; //pos 17
    int ar18[4] = {11, 17, 19, 25}; //pos 18
    int ar19[4] = {12, 18, 20, 26}; //pos 19
    int ar20[3] = {13, 19, 27}; //pos 20
    
    [levelContainer[0] addNeighbors:ar14 tocId:14 withNumberOf:3];
    [levelContainer[0] addNeighbors:ar15 tocId:15 withNumberOf:4];
    [levelContainer[0] addNeighbors:ar16 tocId:16 withNumberOf:4];
    [levelContainer[0] addNeighbors:ar17 tocId:17 withNumberOf:4];
    [levelContainer[0] addNeighbors:ar18 tocId:18 withNumberOf:4];
    [levelContainer[0] addNeighbors:ar19 tocId:19 withNumberOf:4];
    [levelContainer[0] addNeighbors:ar20 tocId:20 withNumberOf:3];
    
    int ar21[3] = {14, 22, 28}; //pos 21
    int ar22[4] = {15, 21, 23, 29}; //pos 22
    int ar23[4] = {16, 22, 24, 30}; //pos 23
    int ar24[4] = {17, 23, 25, 31}; //pos 24
    int ar25[4] = {18, 24, 26, 32}; //pos 25
    int ar26[4] = {19, 25, 27, 33}; //pos 26
    int ar27[3] = {20, 26, 34}; //pos 27
    
    ////////SPECIAL TO LEVEL 0//////////////////
    int zeroar21[3] = {14, 22}; //pos 21
    int zeroar22[4] = {15, 21, 23}; //pos 22
    int zeroar23[4] = {16, 22, 24}; //pos 23
    int zeroar24[4] = {17, 23, 25}; //pos 24
    int zeroar25[4] = {18, 24, 26}; //pos 25
    int zeroar26[4] = {19, 25, 27}; //pos 26
    int zeroar27[3] = {20, 26}; //pos 27
    
    [levelContainer[0] addNeighbors:zeroar21 tocId:21 withNumberOf:2];
    [levelContainer[0] addNeighbors:zeroar22 tocId:22 withNumberOf:3];
    [levelContainer[0] addNeighbors:zeroar23 tocId:23 withNumberOf:3];
    [levelContainer[0] addNeighbors:zeroar24 tocId:24 withNumberOf:3];
    [levelContainer[0] addNeighbors:zeroar25 tocId:25 withNumberOf:3];
    [levelContainer[0] addNeighbors:zeroar26 tocId:26 withNumberOf:3];
    [levelContainer[0] addNeighbors:zeroar27 tocId:27 withNumberOf:2];
    
    
    int ar28[2] = {21, 29}; //pos 28
    int ar29[3] = {22, 28, 30}; //pos 29
    int ar30[3] = {23, 29, 31}; //pos 30
    int ar31[3] = {24, 30, 32}; //pos 31
    int ar32[3] = {25, 31, 33}; //pos 32
    int ar33[3] = {26, 32, 34}; //pos 33
    int ar34[2] = {27, 33}; //pos 34
    
    [levelContainer[0] addNeighbors:ar28 tocId:28 withNumberOf:2];
    [levelContainer[0] addNeighbors:ar29 tocId:29 withNumberOf:3];
    [levelContainer[0] addNeighbors:ar30 tocId:30 withNumberOf:3];
    [levelContainer[0] addNeighbors:ar31 tocId:31 withNumberOf:3];
    [levelContainer[0] addNeighbors:ar32 tocId:32 withNumberOf:3];
    [levelContainer[0] addNeighbors:ar33 tocId:33 withNumberOf:3];
    [levelContainer[0] addNeighbors:ar34 tocId:34 withNumberOf:2];
    
    //////////////////////////////level 2 1th level///////////////////////////////
    int arr2[3] = {1, 8, 9};
    
    [levelContainer[1] addNeighbors:arr2 tocId:0 withNumberOf:3];
    [levelContainer[1] addNeighbors:arr1 tocId:1 withNumberOf:5];
    
    
    [levelContainer[1] addNeighbors:ar0 tocId:0 withNumberOf:2];
    [levelContainer[1] addNeighbors:ar1 tocId:1 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar2 tocId:2 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar3 tocId:3 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar4 tocId:4 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar5 tocId:5 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar6 tocId:6 withNumberOf:2];
    
    
    
    [levelContainer[1] addNeighbors:ar7 tocId:7 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar8 tocId:8 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar9 tocId:9 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar10 tocId:10 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar11 tocId:11 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar12 tocId:12 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar13 tocId:13 withNumberOf:3];
    
    
    [levelContainer[1] addNeighbors:ar14 tocId:14 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar15 tocId:15 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar16 tocId:16 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar17 tocId:17 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar18 tocId:18 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar19 tocId:19 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar20 tocId:20 withNumberOf:3];
    
    
    [levelContainer[1] addNeighbors:ar21 tocId:21 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar22 tocId:22 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar23 tocId:23 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar24 tocId:24 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar25 tocId:25 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar26 tocId:26 withNumberOf:4];
    [levelContainer[1] addNeighbors:ar27 tocId:27 withNumberOf:3];
    
    
    
    [levelContainer[1] addNeighbors:ar28 tocId:28 withNumberOf:2];
    [levelContainer[1] addNeighbors:ar29 tocId:29 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar30 tocId:30 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar31 tocId:31 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar32 tocId:32 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar33 tocId:33 withNumberOf:3];
    [levelContainer[1] addNeighbors:ar34 tocId:34 withNumberOf:2];
    
}

@end
