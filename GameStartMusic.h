//
//  GameStartMusic.h
//  2dGameRemake
//
//  Created by Joseph Reiter on 11/19/12.
//  Copyright (c) 2012 Joseph Reiter. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AudioToolbox/AudioServices.h>

@interface GameStartMusic : AVAudioPlayer{
}

@property NSString *soundFilePath;
@property NSURL *soundFileURL;
@property AVAudioPlayer *player;

-(id)init;
-(void)playGameStartMusic;
-(void)stopGameStartMusic;

@end
